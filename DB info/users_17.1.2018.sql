/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : cabbieappv2

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-01-17 07:57:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(225) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `home_no` varchar(20) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `forgot_pwd_token` varchar(255) DEFAULT NULL,
  `is_enabled` tinyint(1) DEFAULT '1',
  `first_update` tinyint(1) DEFAULT '0',
  `code` varchar(255) DEFAULT NULL,
  `promoter_code` varchar(255) DEFAULT NULL,
  `ref_promoter_code` varchar(255) DEFAULT NULL,
  `promoter_percentage` float DEFAULT NULL,
  `admin_percentage` float DEFAULT NULL,
  `admin_percentage_cash` float(20,0) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `type` varchar(20) DEFAULT NULL COMMENT 'driver or user',
  `voip_no` varchar(20) DEFAULT NULL,
  `running_distance` float DEFAULT '5' COMMENT 'A range set by the driver only how far s/he can travel to pick a passenger',
  `virtual_rank` int(11) DEFAULT NULL,
  `phone_rank` int(11) DEFAULT NULL,
  `gps_rank` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `badge_no` varchar(255) DEFAULT '0',
  `badge_image` varchar(255) DEFAULT NULL,
  `registration_plate_no` varchar(255) DEFAULT '0',
  `insurance_certificate` date DEFAULT NULL,
  `insurance_certificate_image` varchar(255) DEFAULT NULL,
  `vehicle_licence_image` varchar(255) DEFAULT NULL,
  `vehicle_licence_no` varchar(255) DEFAULT NULL,
  `vehicle_type` varchar(50) DEFAULT NULL,
  `is_wheelchair` tinyint(1) DEFAULT NULL,
  `cab_type` varchar(50) DEFAULT NULL,
  `no_of_seat` varchar(50) DEFAULT NULL,
  `lat` varchar(20) DEFAULT NULL,
  `lng` varchar(20) DEFAULT NULL,
  `phonecall_available` varchar(5) DEFAULT 'yes' COMMENT 'yes/no',
  `phonecall_zone` int(11) DEFAULT NULL,
  `phonecall_gps_available` varchar(5) DEFAULT 'yes' COMMENT 'yes / no',
  `phonecall_gps_zone` int(11) DEFAULT NULL,
  `vr_available` varchar(5) DEFAULT 'yes' COMMENT 'yes / no',
  `vr_zone` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT NULL COMMENT 'driver''s position in user table',
  `last_activity` datetime DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `account_holder_name` varchar(255) DEFAULT NULL,
  `iban` varchar(255) DEFAULT NULL,
  `sort_code` varchar(255) DEFAULT NULL,
  `swift` varchar(255) DEFAULT NULL,
  `from_web` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `vendor_license_number` varchar(255) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `vendor_license_exp_date` date DEFAULT NULL,
  `vendor_license_holder_name` varchar(255) DEFAULT NULL,
  `vendor_license_authority` varchar(255) DEFAULT NULL,
  `vendor_license_image` varchar(255) DEFAULT NULL,
  `company_slug` varchar(100) DEFAULT NULL,
  `branch_number` varchar(255) DEFAULT NULL,
  `drivers_licence_image_front` varchar(255) DEFAULT NULL,
  `drivers_licence_image_back` varchar(255) DEFAULT NULL,
  `driving_licence_dateExpiry` date DEFAULT NULL,
  `driving_licence_no` varchar(500) DEFAULT NULL,
  `post_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `mobile` (`mobile`),
  KEY `password` (`password`),
  KEY `code` (`code`),
  KEY `type` (`type`),
  KEY `voip_no` (`voip_no`),
  KEY `running_distance` (`running_distance`),
  KEY `virtual_rank` (`virtual_rank`),
  KEY `phone_rank` (`phone_rank`),
  KEY `gps_rank` (`gps_rank`),
  KEY `vehicle_type` (`vehicle_type`),
  KEY `is_wheelchair` (`is_wheelchair`),
  KEY `no_of_seat` (`no_of_seat`),
  KEY `lat` (`lat`),
  KEY `lng` (`lng`),
  KEY `phonecall_available` (`phonecall_available`),
  KEY `vr_available` (`vr_available`),
  KEY `last_activity` (`last_activity`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('4', 'Raxz', null, 'r@g.com', '01724160299', null, 'e8b8df0441b07c1141a5fd4ec9f3743d90ab4398', null, '1', '0', '5a5e14e2-e864-4231-9aa5-18bc1603f7ec', null, null, null, null, null, '0', 'passenger', null, '5', null, null, null, null, '0', null, '0', null, null, null, null, null, null, null, null, null, null, 'yes', null, 'yes', null, 'yes', null, null, '2018-01-17 01:40:40', null, null, null, null, null, '0', '2018-01-16 15:06:10', '2018-01-17 01:40:40', null, null, null, null, null, null, null, '1234', null, null, null, null, null);
INSERT INTO `users` VALUES ('2', 'Khirul', 'dsd ', 'k@g.com', '01700000000', null, null, null, '1', '0', null, null, null, null, null, null, '0', 'vendor', null, '5', null, null, null, null, '0', null, '0', null, null, null, null, null, null, null, null, '52.953289', '-1.140398', 'yes', null, 'yes', null, 'yes', null, null, null, null, null, null, null, null, '0', '2018-01-16 20:58:21', '2018-01-16 20:58:25', null, 'Taxi Lagbe', null, null, null, null, null, '1234', null, null, null, null, null);
