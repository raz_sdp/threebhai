/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50527
Source Host           : 127.0.0.1:3306
Source Database       : cabbieappukv2

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2016-12-28 19:31:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `home_page`
-- ----------------------------
DROP TABLE IF EXISTS `home_page`;
CREATE TABLE `home_page` (
  `id` varchar(50) DEFAULT NULL,
  `segment` tinyint(2) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `images` longtext,
  `video_iframe` longtext,
  `created` datetime DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('active','inactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of home_page
-- ----------------------------
INSERT INTO `home_page` VALUES ('admin', '1', 'About Cabbie App UK... 1', '<p>\"CabbieAppUk\" is a technology platform that brings together UK\'S Taxi service providers with mobile app users.  The app has been built taking to consideration you might travel to different places in the UK and might need a taxi but not always be able to find one, or a phone number for the cab office.</p> \r\n          \r\n          <p>We work with licenced hackney carriage taxi offices and drivers, and licenced private hire cab offices to give you the better service.  All of our Drivers are Criminal Record Check Approved by their licensing authority.  We intend to have licenced taxis working with Cabbie App UK throughout the country so wherever you are travelling in the United Kingdom you\'ll always be able to find a taxi with us.  If we do not have a taxi in your area when you need it we have a backup plan and will provide you a local taxi number instead.</p>\r\n       \r\n          <p>As long as you have \"Cabbie App UK\" you will never have to worry about a taxi number again anywhere in the UK. </p>', '[\"1.jpg\",\"2.jpg\",\"3.jpg\"]', 'http://www.youtube.com/embed/SEBLt6Kd9EY', '2016-12-28 16:41:53', '2016-12-28 17:57:46', 'active');
INSERT INTO `home_page` VALUES ('admin', '2', 'About Cabbie App UK... 2', '<p>As long as you have \"Cabbie App UK\" you will never have to worry about a taxi number again anywhere in the UK. </p>', '[\"4.jpg\",\"5.jpg\",\"6.jpg\"]', null, '2016-12-28 16:13:49', '2016-12-28 17:37:27', 'active');
INSERT INTO `home_page` VALUES ('admin', '3', 'About Cabbie App UK... 3', '<p>\"CabbieAppUk\" is a technology platform that brings together UK\'S Taxi service providers with mobile app users.  The app has been built taking to consideration you might travel to different places in the UK and might need a taxi but not always be able to find one, or a phone number for the cab office.</p> \r\n          \r\n          <p>We work with licenced hackney carriage taxi offices and drivers, and licenced private hire cab offices to give you the better service.  All of our Drivers are Criminal Record Check Approved by their licensing authority.  We intend to have licenced taxis working with Cabbie App UK throughout the country so wherever you are travelling in the United Kingdom you\'ll always be able to find a taxi with us.  If we do not have a taxi in your area when you need it we have a backup plan and will provide you a local taxi number instead.</p>\r\n       \r\n          <p>As long as you have \"Cabbie App UK\" you will never have to worry about a taxi number again anywhere in the UK. </p>', null, 'http://www.youtube.com/embed/SEBLt6Kd9EY', '2016-12-28 16:13:49', '2016-12-28 18:04:28', 'active');
