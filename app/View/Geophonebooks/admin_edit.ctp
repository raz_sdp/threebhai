<div class="geophonebooks form">
<?php $this->Html->scriptStart(array('inline' => false)); ?>

	var lat = <?php echo $this->request->data['Geophonebook']['lat']; ?>;
	var lng = <?php echo $this->request->data['Geophonebook']['lng']; ?>;

<?php $this->Html->scriptEnd(); ?>
<?php  $this->Html->script(array('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false','jquery-1.10.2.min','phonebook_admin_map_edit'),array('inline' => false)); ?>


<?php echo $this->Form->create('Geophonebook'); ?>
	<fieldset>
		<legend><?php echo __('Edit Geophonebook'); ?></legend>
		<div id="panel">
  <?php echo $this->Form->input('address'); ?>
      <input type="button" value="Geocode" onclick="codeAddress()">
    </div>
    <div id="map-canvas"></div>
	<?php
	    echo $this->Form->input('id');
		echo $this->Form->input('lat',array('label' => 'Latitude'));
		echo $this->Form->input('lng', array('label' => 'Longitude'));
		echo $this->Form->input('phone', array('label' => 'Phone Number'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
