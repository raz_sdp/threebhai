<div class="geophonebooks view">
<h2><?php echo __('Geophonebook'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($geophonebook['Geophonebook']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lat'); ?></dt>
		<dd>
			<?php echo h($geophonebook['Geophonebook']['lat']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lng'); ?></dt>
		<dd>
			<?php echo h($geophonebook['Geophonebook']['lng']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($geophonebook['Geophonebook']['phone']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Geophonebook'), array('action' => 'edit', $geophonebook['Geophonebook']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Geophonebook'), array('action' => 'delete', $geophonebook['Geophonebook']['id']), null, __('Are you sure you want to delete # %s?', $geophonebook['Geophonebook']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Geophonebooks'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Geophonebook'), array('action' => 'add')); ?> </li>
	</ul>
</div>
