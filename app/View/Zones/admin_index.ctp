<script type="text/javascript">
	// zone type selection drop down change fn
	$(function(){
		$('#zone_type').on('change', function(){
			location.href = ROOT + 'admin/zones/index/' + $(this).val();
		});
	});
</script>
<?php
	$zone_types = array(
				'vr_zone' => 'VR Zone',
				'phonecall_gps_zone'=> 'Phone GPS Zone',
				'phonecall_zone' => 'Phonecall Zone'
				
			);
 if($zone_type != 'phonecall_zone'){ ?>
<script type="text/javascript">
	var polygons = JSON.parse('<?php echo json_encode($polygons);?>');
	//var circles = JSON.parse('<?php //echo json_encode($circles);?>');
</script>
<?php echo $this->Html->script(array('http://maps.google.com/maps/api/js?sensor=false','markerwithlabel_packed.js','zones_index')); ?>
<?php } ?>
<div class="zones index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add new '. $zone_types[$zone_type], array('controller' => 'zones', 'action' => 'add', 'admin' => true, 'prefix' => 'admin', $zone_type))?></div>
	<h2><?php echo __('Zones'); ?></h2>
	<?php 
		echo $this->Form->input('zone_type', array('options' => $zone_types,
			'selected' => $zone_type
		)
	);
	
	if($zone_type != 'phonecall_zone'){
	?>
	<div id="main-map">
	</div>
	<?php } ?>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th>Name</th>
			<?php if ($zone_type != 'vr_zone') { ?>
				<th>VOIP Phone No</th>
			<?php } ?>
			<th class="actions"></th>
	</tr>
	<?php foreach ($zones as $zone): ?>
	<tr id="zone_<?php echo $zone['Zone']['id']; ?>">
		<td><?php echo h($zone['Zone']['name']); ?>&nbsp;</td>
		<?php if($zone_type != 'vr_zone') { ?>
		<td><?php echo h($zone['Zone']['voip_phone_no']);?></td>
		<?php } ?>
		<td class="actions">
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $zone['Zone']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $zone['Zone']['id']), null, __('Are you sure you want to delete %s?', $zone['Zone']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
</div>
<?php echo $this->element('menu'); ?>
