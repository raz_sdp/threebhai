<div class="zones index">
	<h2><?php echo __('Available Twilio Numbers'); ?></h2>
                <h3>Choose a Twilio number to buy</h3>
                <table cellpadding="0" cellspacing="0">
                	 <?php foreach($numbers->available_phone_numbers as $number): ?>
                	<tr>
                		<td width="70%"><?php echo $number->friendly_name ?></td>
                		<td>
                			<?php echo $this->Form->postLink(__('Buy'), array('action' => 'admin_buy_twilio_phone_numbers',$number->phone_number), null, __('Are you sure you want to purchase %s Twilio Number?', $number->phone_number)); ?>
                		</td>
                	</tr>
                	<?php endforeach; ?>
                </table>
                <h3>Existing Purchased Twilio numbers</h3>
                <table cellpadding="0" cellspacing="0">
                	 <?php foreach($existing_purchage_twilio_numbers as $existing_purchage_twilio_number): ?>
                	<tr>
                		<td width="70%"><?php echo $existing_purchage_twilio_number['number']; ?></td>
                                <td>
                                        <?php echo $this->Html->link(__('Delete'), array('action' => 'admin_delete_twilio_phone_numbers',$existing_purchage_twilio_number['sid']), null, __('Are you sure you want to delete %s?', $existing_purchage_twilio_number['number'])); ?>
                                </td>
                	</tr>
                	<?php endforeach; ?>
                </table>
</div>
<?php echo $this->element('menu'); ?>