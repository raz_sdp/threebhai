<style>
   form .days div.checkbox{
        float: left;
        clear: none;
    }
</style>
<?php
echo $this->Html->script('booking_holidays', array('inline'=>false));
?>

<div class="vrSettings form">
<?php echo $this->Form->create('VrSetting'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add VR Zone Setting'); ?></legend>
	<?php
        echo $this->Form->input('zone_id', array('label' => 'VR Zone'));
        echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type','empty' => '(Not A Holiday)'));
        echo $this->Form->input('start_date',array(
        'dateFormat'=>'DMY',
        'minYear'=>date('Y')+20,
        'maxYear'=>date('Y')-1,
        ));
        echo $this->Form->input('end_date',array(
        'dateFormat'=>'DMY',
        'minYear'=>date('Y')+20,
        'maxYear'=>date('Y')-1,
        ));
        echo $this->Form->input('start_time');
        echo $this->Form->input('end_time');
        
        $weekdays =  array('0' => 'Sunday', '1'=> 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
		?>
		<div class='days'>
		<?php
       	echo $this->Form->input('week_no', array('label' => false ,'multiple' => 'checkbox', 'options' => $weekdays));
       	?>
    </div>
    <?php
        echo  $this->Form->input('is_advance_booking_on', array('checked' => true));
    ?>
    <?php
       // echo  $this->Form->input('min_advance_booking_time');
    ?>	
	<?php
	//	echo $this->Form->input('is_over_job_notification', array('checked' => true));
	?>
	<?php
        echo $this->Form->input('over_job_amount', array('label' => 'Over Job Amount (If empty, no email will be sent)'));
        ?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
