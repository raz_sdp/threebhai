<div class="vrSettings view">
<h2><?php  echo __('Booking Setting'); ?></h2>
	<dl>
        <dt><?php echo __('Zone'); ?></dt>
        <dd>
            <?php echo $this->Html->link($vrSetting['Zone']['name'], array('controller' => 'zones', 'action' => 'view', $vrSetting['Zone']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Start Date'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['start_date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('End Date'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['end_date']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Start Time'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['start_time']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('End Time'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['end_time']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Week No'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['week_no']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Is Advance Booking On'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['is_advance_booking_on']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('Min Advance Booking Time'); ?></dt>
		<dd>
			<?php echo h($vrSetting['VrSetting']['min_advance_booking_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Over Job Notification'); ?></dt>
		<dd>
			<?php echo h($vrSetting['VrSetting']['is_over_job_notification']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Over Job Amount'); ?></dt>
		<dd>
			<?php echo h($vrSetting['VrSetting']['over_job_amount']); ?>
			&nbsp;
		</dd>
        <dt><?php echo __('Repeat Call Duration'); ?></dt>
        <dd>
            <?php echo h($vrSetting['VrSetting']['repeat_call_duration']); ?>
            &nbsp;
        </dd>
		<dt><?php echo __('Promoter Percentage'); ?></dt>
		<dd>
			<?php echo h($vrSetting['VrSetting']['promoter_percentage']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>