<div class="vrSettings index">
    <div class="AddNewButton"><?php echo $this->Html->link('Add New VR Zone Setting', array('controller' => 'vr_settings', 'action' => 'add', 'admin' => true));?></div>
	<h2><?php echo __('VR Zone Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
        <th><?php echo $this->Paginator->sort('zone_id','VR Zone'); ?></th>
        <th><?php echo $this->Paginator->sort('holidaytype_id','Holiday Type'); ?></th>
        <th><?php echo $this->Paginator->sort('start_date'); ?></th>
        <th><?php echo $this->Paginator->sort('end_date'); ?></th>
        <th><?php echo $this->Paginator->sort('start_time'); ?></th>
        <th><?php echo $this->Paginator->sort('end_time'); ?></th>
        <th><?php echo $this->Paginator->sort('week_no','Days'); ?></th>
        <th><?php echo $this->Paginator->sort('is_advance_booking_on','Advanced Booking On'); ?></th>
        <th><?php echo $this->Paginator->sort('over_job_amount'); ?></th>
        <th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vrSettings as $vrSetting): ?>
	<tr>
        <td><?php 
        if($vrSetting['VrSetting']['zone_id'] == null) {
        	echo 'Default';
        } else echo h($vrSetting['Zone']['name']);
        ?></td>
        <td><?php if($vrSetting['Holidaytype']['holiday_type'] != Null) { 
        echo h($vrSetting['Holidaytype']['holiday_type']); 
    	} else echo h('N/A'); 
        ?></td>
    	<?php  
		    if ($vrSetting['VrSetting']['start_date'] != Null && $vrSetting['VrSetting']['end_date'] != Null) {
				$s_date = date_create($vrSetting['VrSetting']['start_date']);
				$e_date = date_create($vrSetting['VrSetting']['end_date']);
		?>
			<td><?php echo date_format($s_date,'d/m/Y'); ?>&nbsp;</td>
	        <td><?php echo date_format($e_date,'d/m/Y'); ?>&nbsp;</td>
			<?php
				} else { ?>
					<td><?php echo ('N/A'); ?>&nbsp;</td>
		        	<td><?php echo ('N/A'); ?>&nbsp;</td>
				<?php }
					$s_time = date_create($vrSetting['VrSetting']['start_time']);
					$e_time = date_create($vrSetting['VrSetting']['end_time']);
				?>
	        <td><?php if(empty($vrSetting['VrSetting']['start_time'])) echo 'N/A';
	        		else echo date_format($s_time, 'g:i A'); 
	         ?>&nbsp;</td>
    	    <td><?php if(empty($vrSetting['VrSetting']['end_time'])) echo 'N/A';
    	     else echo date_format($e_time, 'g:i A'); 
    	     ?>&nbsp;</td>
    	    <?php if($vrSetting['VrSetting']['week_no'] != Null) { 
    	    	$weekdays =  array('0' => 'Sunday', '1' => 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
				$day = trim($vrSetting['VrSetting']['week_no'], '#');
				$wks = explode('#', $day);
			?>
        	<td><?php foreach ($wks as $key) {
				echo $weekdays [$key];?>
				<br><?php
				} 
				?>&nbsp;</td>
			<?php } else { ?>
		 		<td><?php echo ('N/A'); ?>&nbsp;</td>
			<?php } ?>
        <td><?php 
			if(!empty($vrSetting['VrSetting']['is_advance_booking_on'])) {
				echo h('Yes'); 
			} else {
				echo h('No');
			} 
         	?>
         &nbsp;</td>
        <td><?php
         if(!empty($vrSetting['VrSetting']['over_job_amount'])) {
				echo h($vrSetting['VrSetting']['over_job_amount']); 
			} else {
				echo h('No');
			} 
         	?>
         &nbsp;
         </td>
		<td class="actions">
			<?php 
			if($vrSetting['VrSetting']['zone_id'] == null) {
				echo 'Not Allowed' ;
			} else {
				echo $this->Html->link(__('Edit'), array('action' => 'edit', $vrSetting['VrSetting']['id'])); 
				if(empty($vrSetting['Holidaytype']['holiday_type'])) {
					echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vrSetting['VrSetting']['id']), null, __('Are you sure you want to delete %s?', $vrSetting['Zone']['name']));
				} else {
					echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $vrSetting['VrSetting']['id']), null, __('Are you sure you want to delete %s for %s?', $vrSetting['Zone']['name'], $vrSetting['Holidaytype']['holiday_type']));

				}
			}
			?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>