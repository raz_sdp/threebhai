<div class="bookingSettings index">
	<h2><?php echo __('Booking Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('min_advance_booking_time'); ?></th>
			<th><?php echo $this->Paginator->sort('is_advance_booking_on'); ?></th>
			<th><?php echo $this->Paginator->sort('week_no'); ?></th>
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>
			<th><?php echo $this->Paginator->sort('zone_ids'); ?></th>
			<th><?php echo $this->Paginator->sort('is_over_job_notification'); ?></th>
			<th><?php echo $this->Paginator->sort('over_job_amount'); ?></th>
			<th><?php echo $this->Paginator->sort('promoter_percentage'); ?></th>
			<th><?php echo $this->Paginator->sort('repeat_call_duration'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bookingSettings as $bookingSetting): ?>
	<tr>
		<td><?php echo h($bookingSetting['BookingSetting']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bookingSetting['User']['name'], array('controller' => 'users', 'action' => 'view', $bookingSetting['User']['id'])); ?>
		</td>
		<td><?php echo h($bookingSetting['BookingSetting']['min_advance_booking_time']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['is_advance_booking_on']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['week_no']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['end_date']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($bookingSetting['Zones']['id'], array('controller' => 'zones', 'action' => 'view', $bookingSetting['Zones']['id'])); ?>
		</td>
		<td><?php echo h($bookingSetting['BookingSetting']['is_over_job_notification']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['over_job_amount']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['promoter_percentage']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['repeat_call_duration']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['created']); ?>&nbsp;</td>
		<td><?php echo h($bookingSetting['BookingSetting']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $bookingSetting['BookingSetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $bookingSetting['BookingSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $bookingSetting['BookingSetting']['id']), null, __('Are you sure you want to delete # %s?', $bookingSetting['BookingSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Booking Setting'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zones'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
