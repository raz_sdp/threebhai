<?php 
		$sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout', 'request_sent' => 'Request Sent'); 
	?>
<div class="archivedBookings view">
<h2><?php echo __('Cancelled Booking '); echo h($archivedBooking['ArchivedBooking']['id']); ?></h2>
	<dl>
		<dt><?php echo __('Pick Up'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['pick_up']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['destination']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('At Time'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['at_time']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('SAP Booking'); ?></dt>
		<dd>
			<?php if(($archivedBooking['ArchivedBooking']['is_sap_booking']) == '1') echo 'Yes'; 
			else 'No';
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Passengers'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['persons']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fare'); ?></dt>
		<dd>
			<?php echo $this->Number->currency($archivedBooking['ArchivedBooking']['fare'], 'GBP'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Real Fare'); ?></dt>
		<dd>
			<?php echo $this->Number->currency($archivedBooking['ArchivedBooking']['real_fare'], 'GBP'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requester'); ?></dt>
		<dd>
			<?php echo $this->Html->link($archivedBooking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $archivedBooking['ArchivedBooking']['requester'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acceptor'); ?></dt>
		<dd>
			<?php 
				if(!empty($archivedBooking['ArchivedBooking']['acceptor'])) {
					echo $this->Html->link($archivedBooking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $archivedBooking['ArchivedBooking']['acceptor'])); 
				} else {
					if(empty($archivedBooking['ArchivedBooking']['status'])) echo 'Queued';
					elseif ($archivedBooking['ArchivedBooking']['status'] =='no_driver_found' AND $archivedBooking['ArchivedBooking']['is_sap_booking'] == '1' AND $archivedBooking['ArchivedBooking']['from_web']!='1') {
						echo 'Cancelled';
					}elseif($archivedBooking['ArchivedBooking']['status'] =='no_driver_found' AND $archivedBooking['ArchivedBooking']['is_sap_booking'] == '1' AND $archivedBooking['ArchivedBooking']['from_web']=='1') {
						echo 'No Driver Found';
					}else {
						$val = $archivedBooking['ArchivedBooking']['status'];
						echo $sts[$val]; 
					}
				}
			?>&nbsp;
		</dd>
		<dt><?php echo __('Hunting No'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['hunting_no']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Special Requirement'); ?></dt>
		<dd>
			<?php 
			if (!empty($archivedBooking['ArchivedBooking']['special_requirement'])){
				echo h($archivedBooking['ArchivedBooking']['special_requirement']); 
			} else {
				echo 'N/A';
			}
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note To Driver'); ?></dt>
		<dd>
			<?php echo h($archivedBooking['ArchivedBooking']['note_to_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Advanced Booking'); ?></dt>
		<dd>
			<?php 
			if ($archivedBooking['ArchivedBooking']['is_pre_book'] == '1'){
				echo 'Yes';
			} else {
				echo 'No';
			}
			?>&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php 
			$date_time = date_create($archivedBooking['ArchivedBooking']['created']);
			echo date_format($date_time,'d/m/Y g:i A');
			?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>
