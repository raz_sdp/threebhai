<div class="archivedBookings index">
	<h2><?php echo __('Archived Bookings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_lat'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_lng'); ?></th>
			<th><?php echo $this->Paginator->sort('destination'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_lat'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_lng'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_airport'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_airport'); ?></th>
			<th><?php echo $this->Paginator->sort('zone_id'); ?></th>
			<th><?php echo $this->Paginator->sort('total_distance'); ?></th>
			<th><?php echo $this->Paginator->sort('distance_google'); ?></th>
			<th><?php echo $this->Paginator->sort('at_time'); ?></th>
			<th><?php echo $this->Paginator->sort('persons'); ?></th>
			<th><?php echo $this->Paginator->sort('car_type'); ?></th>
			<th><?php echo $this->Paginator->sort('via'); ?></th>
			<th><?php echo $this->Paginator->sort('fare'); ?></th>
			<th><?php echo $this->Paginator->sort('real_fare'); ?></th>
			<th><?php echo $this->Paginator->sort('is_paid'); ?></th>
			<th><?php echo $this->Paginator->sort('requester'); ?></th>
			<th><?php echo $this->Paginator->sort('customer_phn_no'); ?></th>
			<th><?php echo $this->Paginator->sort('acceptor'); ?></th>
			<th><?php echo $this->Paginator->sort('accept_time'); ?></th>
			<th><?php echo $this->Paginator->sort('hunting_no'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('special_requirement'); ?></th>
			<th><?php echo $this->Paginator->sort('bar_driver'); ?></th>
			<th><?php echo $this->Paginator->sort('note_to_driver'); ?></th>
			<th><?php echo $this->Paginator->sort('booking_type'); ?></th>
			<th><?php echo $this->Paginator->sort('is_pre_book'); ?></th>
			<th><?php echo $this->Paginator->sort('is_sap_booking'); ?></th>
			<th><?php echo $this->Paginator->sort('notify_admin'); ?></th>
			<th><?php echo $this->Paginator->sort('route'); ?></th>
			<th><?php echo $this->Paginator->sort('recurring'); ?></th>
			<th><?php echo $this->Paginator->sort('recurring_start'); ?></th>
			<th><?php echo $this->Paginator->sort('recurring_end'); ?></th>
			<th><?php echo $this->Paginator->sort('no_of_luggages'); ?></th>
			<th><?php echo $this->Paginator->sort('from_web'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($archivedBookings as $archivedBooking): ?>
	<tr>
		<td><?php echo h($archivedBooking['ArchivedBooking']['id']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up_lat']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up_lng']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination_lat']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination_lng']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination_postcode']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['destination_airport']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up_postcode']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['pick_up_airport']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($archivedBooking['Zone']['name'], array('controller' => 'zones', 'action' => 'view', $archivedBooking['Zone']['id'])); ?>
		</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['total_distance']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['distance_google']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['at_time']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['persons']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['car_type']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['via']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['fare']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['real_fare']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['is_paid']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['requester']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['customer_phn_no']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['acceptor']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['accept_time']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['hunting_no']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['status']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['special_requirement']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['bar_driver']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['note_to_driver']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['booking_type']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['is_pre_book']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['is_sap_booking']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['notify_admin']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['route']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['recurring']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['recurring_start']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['recurring_end']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['no_of_luggages']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['from_web']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['created']); ?>&nbsp;</td>
		<td><?php echo h($archivedBooking['ArchivedBooking']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $archivedBooking['ArchivedBooking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $archivedBooking['ArchivedBooking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $archivedBooking['ArchivedBooking']['id']), null, __('Are you sure you want to delete # %s?', $archivedBooking['ArchivedBooking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Archived Booking'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Zones'), array('controller' => 'zones', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Zone'), array('controller' => 'zones', 'action' => 'add')); ?> </li>
	</ul>
</div>
