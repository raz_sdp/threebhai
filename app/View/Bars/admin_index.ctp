<div class="bars index">
	<h2><?php echo __('Bars'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('userId1','Who'); ?></th>
			<th><?php echo $this->Paginator->sort('userId2','Whom'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($bars as $bar): ?>
	<tr>
		<td><?php echo h($bar['User1']['name']).' ('. $bar['User1']['type'] .')';?>&nbsp;</td>
		<td><?php echo h($bar['User2']['name']).' ('. $bar['User2']['type'] .')';?>&nbsp;</td>
		
		<?php  
				$created_date = date_create($bar['Bar']['created']);
		?>
		<td><?php echo date_format($created_date,'d/m/Y'); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Form->postLink(__('Un-Bar'), array('action' => 'delete', $bar['Bar']['id']), null, __('Are you sure you want to un-bar %s?', $bar['User2']['name'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>