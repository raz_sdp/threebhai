<div class="holidays form">
<?php echo $this->Form->create('Holiday'); ?>
	<fieldset>
		<legend><?php echo __('Add Holiday'); ?></legend>
	<?php
		echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type'));
		echo $this->Form->input('holiday');
		echo $this->Form->input('is_repeat', array('label' => 'Repeat Every Year'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
