<div class="driverDeclineReasons index">
	<div class="AddNewButton"><?php echo $this->Html->link('Add Driver Decline Reason', array('controller' => 'driver_decline_reasons', 'action' => 'add', 'admin' => true, 'prefix' => 'admin'))?></div>
	<h2><?php echo __('Driver Decline Reasons'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('reason'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active','Active'); ?></th>
			<th class="actions"><?php echo __(''); ?></th>
	</tr>
	<?php foreach ($driverDeclineReasons as $driverDeclineReason): ?>
	<tr>
		<td><?php echo h($driverDeclineReason['DriverDeclineReason']['reason']); ?>&nbsp;</td>
		<td><?php 
		if($driverDeclineReason['DriverDeclineReason']['is_active'] == '1') {
			echo 'Yes';
		} else echo 'No';
		?>
		</td>
		<td class="actions">
			<?php
			if($driverDeclineReason['DriverDeclineReason']['is_active'] == 0){
				echo $this->Html->link(__('Active'), array( 'action' => 'active_reason', $driverDeclineReason['DriverDeclineReason']['id']), array('div' => false, 'class' => 'e_d', 'style="padding: 2px 13px;"')); 
			} else {
				echo $this->Html->link(__('Deactive'), array('action' => 'deactive_reason', $driverDeclineReason['DriverDeclineReason']['id']), array('div' => false, 'class' => 'd_d')); 
			}
			?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $driverDeclineReason['DriverDeclineReason']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $driverDeclineReason['DriverDeclineReason']['id']), null, __('Are you sure you want to delete : %s?', $driverDeclineReason['DriverDeclineReason']['reason'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>
