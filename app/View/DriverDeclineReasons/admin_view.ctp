<div class="driverDeclineReasons view">
<h2><?php  echo __('Driver Decline Reason'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($driverDeclineReason['DriverDeclineReason']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reason'); ?></dt>
		<dd>
			<?php echo h($driverDeclineReason['DriverDeclineReason']['reason']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Driver Decline Reason'), array('action' => 'edit', $driverDeclineReason['DriverDeclineReason']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Driver Decline Reason'), array('action' => 'delete', $driverDeclineReason['DriverDeclineReason']['id']), null, __('Are you sure you want to delete # %s?', $driverDeclineReason['DriverDeclineReason']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Driver Decline Reasons'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Driver Decline Reason'), array('action' => 'add')); ?> </li>
	</ul>
</div>
