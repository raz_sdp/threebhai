
<div id="overlay"></div>
   <div class="container-fluid">
     <div class="section-one" id="intro">
       <div class="container">
         <div class="row">
           <div class="icon">
           <?php
           echo $this->Html->link($this->Html->image('message.png', array('alt' => 'CakePHP', 'id'=>'msg')),'mailto:'. $header_links[0]['Setting']['website_contact_email'], array('escape' => false));?>

              <?php echo $this->Html->link($this->Html->image('facebook.png', array('alt' => 'CakePHP', 'id'=>'fb')),$header_links[0]['Setting']['facebook_link'], array('escape' => false, 'target' => '_blank'));?>

              <?php echo $this->Html->link($this->Html->image('twitter.png', array('alt' => 'CakePHP', 'id'=>'twtr')),$header_links[0]['Setting']['twitter_link'], array('escape' => false, 'target' => '_blank'));?>

              <a href="#" class="login-link login">Login</a>

           </div>
         </div>
       </div>
       <div class="bg-color-overlay">
         <div class="container">
           <div class="row">
             <div class="col-md-6  col-sm-6 col-xs-6">
               <h2 class="text-center">The BEST Taxi and Mini Cab Booking App in the UK.</h2>
             </div>
             <div class="col-md-6  col-sm-6 col-xs-6" id="logo">
               <?php echo $this->Html->link($this->Html->image('logo.png', array('alt' => 'CakePHP')),'home', array('escape' => false));?>
             </div>
           </div>
         </div>
       </div>
     </div>
    <section data-type="background" data-speed="10" class="row parttwo" id="second">
            <div class="row">
              <div class="container">
                <div class="conditions-des jscrollpane">
                 <h2> <?php echo($page['title']) ?></h2>
                  <?php echo($page['content']) ?>
                </div>
           </div>
            </div>
        </section>
        <div class="popup" style="display:none">
              <div class="header">
                <?php echo $this->Html->image('delete-btn.png', array('alt' => 'CakePHP', 'class' => 'delete')) ?>
                <h1 class="popup-font text-center">You're almost done!</h1>
                <p class="text-center">To complete your booking you need to have an account with us.</p>
                <p class="text-center">You can access your account online or via our App.</p>

              </div>
              <div class="functionality">

              </div>
                  <?php //echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>

                  <div class="col-md-5 col-sm-5">
                    <div class="row">
                       <h3 class="loginlabel">Login</h3>
                       <form class="form-horizontal login" role="form">
                        <div class="form-group">
                          <label class="col-sm-4 control-label label-left">Email</label>
                          <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" placeholder="Email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-sm-4 control-label label-left">Password</label>
                          <div class="col-sm-8">
                            <input type="password" class="form-control" id="loginpassword" placeholder="password">
                          </div>
                        </div>
                         <div class="form-group">
                          <label class="col-sm-4 control-label label-left"></label>
                          <div class="col-sm-8">
                           <?php echo $this->Html->image('loginbtn.png', array('alt' => 'CakePHP', 'id'=>'loginimg')) ?>
                          </div>
                        </div>

                          </form>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 text-center">
                  <?php echo $this->Html->image('partition.png', array('alt' => 'CakePHP', 'id'=>'partition')) ?>
                </div>
                <div class="col-md-5 col-sm-5">
                <div class="row">
                   <form class="form-horizontal signUp" role="form">
                  <h3 class="signlabel">Sign Up</h3>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Name</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="name" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Mobile No</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="mobileno" placeholder="Mobile No">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Email</label>
                    <div class="col-sm-8">
                    <input type="email" class="form-control" id="signemail" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Password</label>
                    <div class="col-sm-8">
                    <input type="password" class="form-control" id="signpassword" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left">Cofirm Password</label>
                    <div class="col-sm-8">
                    <input type="password" class="form-control" id="cp" placeholder="Retype Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label label-left"></label>
                    <div class="col-sm-8">
                     <?php echo $this->Html->image('signupbtn.png', array('alt' => 'CakePHP', 'id'=>'signimg')) ?>
                    </div>
                  </div>
                </div>

              </form>
              </div>
            </div>
     </div>

        <?php echo $this->element('footer', array('footer' => $footer, 'links' => $links)); ?>

 </div>