<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Set Share Link & Email'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('itunes_link', array('label' => 'Set iTunes Link'));
		echo $this->Form->input('googleplay_link', array('label' => 'Set Google Play Link'));
		echo $this->Form->input('facebook_link', array('label' => 'Set Facebook Link'));
		echo $this->Form->input('twitter_link', array('label' => 'Set Twitter Link'));
		echo $this->Form->input('website_contact_email', array('label' => 'Set Website Contact Email Id'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>