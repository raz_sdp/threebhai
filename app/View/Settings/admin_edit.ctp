<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('free_credit_driver', array('label' => 'Free Credit Amount to be Given for Sign Up to Drivers £'));
		echo $this->Form->input('free_credit_passenger', array('label' => 'Free Credit Amount to be Given for Sign Up to Passengers £'));
		echo $this->Form->input('admin_percentage', array('label' => 'Admin Percentage %'));
		echo $this->Form->input('admin_percentage_cash', array('label' => 'Admin Percentage % for Cash Payment'));
		echo $this->Form->input('promoter_percentage', array('label' => 'Promoter Percentage %'));
		echo $this->Form->input('social_sharing_msg', array('label' => 'Set Dialog on the Social Service'));
		echo $this->Form->input('admin_email', array('label' => 'Set Admin Email'));
		echo $this->Form->input('balance_trans', array('label' => 'Set Minimum Transferable Balance £'));
		echo $this->Form->input('driver_min_balance', array('label' => 'Set Minimum Balance £ Required By Driver to Get a Job'));
		echo $this->Form->input('driver_msg', array('label' => 'Set Small Note for Driver'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>