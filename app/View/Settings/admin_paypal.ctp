<?php
$option = array('Sandbox' => 'Sandbox', 'Live' => 'Live', 'Mock' => 'Mock');
?>
<div class="settings form">
<?php echo $this->Form->create('Setting'); ?>
	<fieldset>
		<legend><?php echo __('Admin PayPal Setting'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('live_username', array('label' => 'Live Username'));
		echo $this->Form->input('paypal_username_api', array('label' => 'API Username for Live'));
		echo $this->Form->input('paypal_password', array('label' => 'API Password for Live'));
		echo $this->Form->input('paypal_signature', array('label' => 'API Signature for Live'));
		echo $this->Form->input('paypal_clientId', array('label' => 'Client Id for Live'));
		echo $this->Form->input('paypal_secret', array('label' => 'Secret Key for Live'));
		echo $this->Form->input('sandbox_username', array('label' => 'Sandbox Username'));
		echo $this->Form->input('sandbox_clientId', array('label' => 'Client Id for Sandbox'));
		echo $this->Form->input('sandbox_secret', array('label' => 'Secret Key for Sandbox'));
		echo $this->Form->input('sandbox_username_api', array('label' => 'Sandbox API Username'));
		echo $this->Form->input('sandbox_password', array('label' => 'Sandbox API Password'));
		echo $this->Form->input('sandbox_signature', array('label' => 'Sandbox API Signature'));
		echo $this->Form->input('paypal_mode', array('options' => $option, 'empty' => '(Select One)'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>