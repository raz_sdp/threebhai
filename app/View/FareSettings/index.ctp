<div class="fareSettings index">
	<h2><?php echo __('Fare Settings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('from_postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('from_airport'); ?></th>
			<th><?php echo $this->Paginator->sort('to_postcode'); ?></th>
			<th><?php echo $this->Paginator->sort('to_airport'); ?></th>
			<th><?php echo $this->Paginator->sort('distance'); ?></th>
			<th><?php echo $this->Paginator->sort('weeks'); ?></th>
			<th><?php echo $this->Paginator->sort('time_from'); ?></th>
			<th><?php echo $this->Paginator->sort('time_to'); ?></th>
			<th><?php echo $this->Paginator->sort('s_4'); ?></th>
			<th><?php echo $this->Paginator->sort('s_5'); ?></th>
			<th><?php echo $this->Paginator->sort('s_6'); ?></th>
			<th><?php echo $this->Paginator->sort('s_7'); ?></th>
			<th><?php echo $this->Paginator->sort('s_8'); ?></th>
			<th><?php echo $this->Paginator->sort('e_4'); ?></th>
			<th><?php echo $this->Paginator->sort('e_5'); ?></th>
			<th><?php echo $this->Paginator->sort('e_6'); ?></th>
			<th><?php echo $this->Paginator->sort('e_7'); ?></th>
			<th><?php echo $this->Paginator->sort('e_8'); ?></th>
			<th><?php echo $this->Paginator->sort('w_3'); ?></th>
			<th><?php echo $this->Paginator->sort('w_4'); ?></th>
			<th><?php echo $this->Paginator->sort('w_5'); ?></th>
			<th><?php echo $this->Paginator->sort('w_6'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($fareSettings as $fareSetting): ?>
	<tr>
		<td><?php echo h($fareSetting['FareSetting']['id']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['from_postcode']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['from_airport']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['to_postcode']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['to_airport']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['distance']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['weeks']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['time_from']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['time_to']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['s_4']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['s_5']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['s_6']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['s_7']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['s_8']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['e_4']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['e_5']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['e_6']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['e_7']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['e_8']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['w_3']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['w_4']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['w_5']); ?>&nbsp;</td>
		<td><?php echo h($fareSetting['FareSetting']['w_6']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $fareSetting['FareSetting']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $fareSetting['FareSetting']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $fareSetting['FareSetting']['id']), null, __('Are you sure you want to delete # %s?', $fareSetting['FareSetting']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Fare Setting'), array('action' => 'add')); ?></li>
	</ul>
</div>
