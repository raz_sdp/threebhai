<style>
	form div.checkbox{
		float: left;
		clear: none;
	}
</style>
<?php
	echo $this->Html->script('fare_holidays', array('inline'=>false));
?>
<div class="fareSettings form">
<?php echo $this->Form->create('FareSetting'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Fixed Fare'); ?></legend>
	<?php
		echo $this->Form->input('pick_up',array('label' =>'From (Airport/Post code)'));
		echo $this->Form->input('destination',array('label' =>'To (Airport/Post code)'));
		echo $this->Form->input('note', array('label' => 'Note For Passenger & Driver'));
		echo $this->Form->input('holidaytype_id', array('label' => 'Holiday Type','empty' => '(Not A Holiday)'));
		$weekdays =  array('0' => 'Sunday', '1'=> 'Monday','2' => 'Tuesday','3' => 'Wednesday','4' => 'Thrusday','5' => 'Friday','6' => 'Saturday');
      	echo $this->Form->input('weeks', array('label' => false ,'multiple' => 'checkbox', 'options' => $weekdays, 'div' => false));
		echo $this->Form->input('time_from', array('label' => 'Start Time'));
		echo $this->Form->input('time_to', array('label' => 'End Time'));
		echo $this->Form->input('fare_setting_type',array('value' => 'fixed_fare', 'type'=>'hidden'));
		echo $this->Form->input('s_4', array('label' => 'Standard Base Fare Up To 4 Passengers (Number Only)', 'style' => 'width:150px; height:30px; padding:0'));
		?><label><strong>Standard Car</strong></label>Up To 5 Passengers <?php
		echo $this->Form->input('s_5', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 6 Passengers <?php
		echo $this->Form->input('s_6', array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 7 Passengers <?php
		echo $this->Form->input('s_7', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?><br><br>Up To 8 Passengers <?php
		echo $this->Form->input('s_8', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?><label><strong>Estate Car</strong></label><?php
		?>Up To 4 Passengers <?php
		echo $this->Form->input('e_4', array('label' => false, 'style' => 'width:175px; height:30px; padding:0','style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 5 Passengers <?php
		echo $this->Form->input('e_5', array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 6 Passengers <?php
		echo $this->Form->input('e_6', array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?><br><br>Up To 7 Passengers <?php
		echo $this->Form->input('e_7', array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 8 Passengers <?php
		echo $this->Form->input('e_8', array('label' => false,'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?><label><strong>Wheelchair</strong></label>Up To 3 Passengers <?php
		echo $this->Form->input('w_3', array('label' => false,'style' => 'width:150px; height:30px; padding:0','div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 4 Passengers <?php
		echo $this->Form->input('w_4', array('label' => false, 'style' => 'width:150px; height:30px; padding:0','div' => false));
		?>&nbsp;&nbsp;&nbsp;&nbsp;Up To 5 Passengers <?php
		echo $this->Form->input('w_5', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'div' => false));
		?><br><br>Up To 6 Passengers <?php
		echo $this->Form->input('w_6', array('label' => false, 'style' => 'width:150px; height:30px; padding:0', 'div' => false));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>
