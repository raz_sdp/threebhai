<style>
	form div.checkbox{
		float: left;
		clear: none;
		width: 300px;
	}
</style>
<?php 
	echo $this->Html->css(array('jquery-ui', 'style-ui'));
	echo $this->Html->script(array('jquery-ui')); 
?>
<script type="text/javascript">
	$(function(){
		$('#insurance_certificate').datepicker({dateFormat : 'dd/mm/yy'});
	}); 
	$( window ).load(function() {
		setTimeout(function () {
			$('input#UserPassword').val('');
       		$('input[type=text]').prop('autocomplete', 'off').val('');
   		}, 1000);
	});
</script> 
<div class="users form">
<?php echo $this->Form->create('User',array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Add new User'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('address');
		echo $this->Form->input('mobile');
		if($type == 'driver') { 
			echo $this->Form->input('voip_no',array('label' => 'Phone Number for Answering Passengers Phone Calls (VOIP No)'));
			echo $this->Form->input('ref_promoter_code', array('label' => '<strong>Reference Promoter Code</strong>','type' => 'select', 'options' => $ref_prom,'empty' => '(Select One)'));
		}
		echo $this->Form->input('home_no');
		echo $this->Form->input('password');
		// echo $this->Form->input('is_enabled');
		echo $this->Form->input('code', array('type' => 'hidden'));
		//echo $this->Form->input('priority');
		echo $this->Form->input('type',array('value' => $type, 'type'=>'hidden'));
	if($type == 'driver'){
		echo $this->Form->input('account_holder_name', array('label' => "Account Holder's Name")); 
		echo $this->Form->input('account_no'); 
		echo $this->Form->input('sort_code'); 
		echo $this->Form->input('iban', array('label' => 'IBAN')); 
		echo $this->Form->input('swift', array('label' => 'SWIFT/BIC code')); 
		echo $this->Form->input('imagex',array('type' =>'file', 'label' => 'Your Image'));
		echo $this->Form->input('badge_imagex',array('type' =>'file','label' => 'Image of Driver Badge'));
		echo $this->Form->input('cab_type',array('options' => array('Private Hire' => 'Private Hire', 'Hackney Carriage' => 'Hackney Carriage'), 'empty' => '(Select One)'));
		echo $this->Form->input('vehicle_type',array('label' => 'Car Type', 'options' => array('Saloon' => 'Saloon', 'Estate' => 'Estate'), 'empty' => '(Select One)'));
		echo $this->Form->input('registration_plate_no');
		echo $this->Form->input('no_of_seat',array('options' => array('2' => '2 Seats', '4' => '4 Seats', '5' => '5 Seats', '6' => '6 Seats', '7' => '7 Seats', '8' => '8 Seats'), 'empty' => '(Select One)'));
		echo $this->Form->input('is_wheelchair',array('label' => 'Wheelchair Access?'));
		echo $this->Form->input('vehicle_licence_no');
		echo $this->Form->input('vehicle_licence_imagex',array('type' =>'file', 'label' => 'Vehicle Licence Image'));
		echo $this->Form->input('insurance_certificate',array('label' => 'Insurance Date Of Expiry', 'id'=> 'insurance_certificate', 'type' => 'text', 'style' => 'width:175px; height:30px; padding:0'));
		echo $this->Form->input('insurance_certificate_imagex',array('type' =>'file', 'label' => 'Insurance Certificate Image'));
		echo $this->Form->input('running_distance', array('label' => 'Running Distance (miles)'));
		echo $this->Form->input('promoter_percentage',array('label' => 'Override Promoter Percentage %'));
		echo $this->Form->input('admin_percentage',array('label' => 'Override Admin Percentage %'));
		echo $this->Form->input('admin_percentage_cash',array('label' => 'Override Admin Percentage % For Cash Payment'));
		echo $this->Form->input('Zone', array('hiddenField' => false, 'label' => '<strong>VR Zone (If ticked NOT allowed)</strong>','type' => 'select', 'multiple' => 'checkbox', 'options' => $vr_zones));
		echo $this->Form->input('Zone', array('hiddenField' => false, 'label' => '<strong>Phone GPS Zone (If ticked allowed)</strong>','type' => 'select', 'multiple' => 'checkbox', 'options' => $phonecall_gps_zones));
		echo $this->Form->input('Zone', array('hiddenField' => false, 'label' => '<strong>Phonecall Zone (If selected allowed)</strong>','type' => 'select', 'options' => $phonecall_zones,'empty' => '(Select One)'));
	}
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>