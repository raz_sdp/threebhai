<?php echo $this->Html->script('signup');?>
<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Register'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		//echo $this->Form->input('address');
		echo $this->Form->input('mobile');
		//echo $this->Form->input('home_no');
		echo $this->Form->input('password');
		echo $this->Form->input('passwordx', array('type' => 'password', 'label' => 'Confirm Password'));
		echo $this->Form->input('code', array('type' => 'hidden'));
		echo $this->Form->input('from_web', array('value' => '1' ,'type' => 'hidden'));
		echo $this->Form->input('type',array('value' => 'passenger', 'type'=>'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>