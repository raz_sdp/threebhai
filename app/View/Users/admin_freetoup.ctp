<div class="topUps form">
<?php echo $this->Form->create('Transaction'); ?>
	<fieldset>
		<legend>Give Free Credit To <?php echo ($user[0]['User']['name']); ?> (<?php echo $user[0]['User']['type']; ?>) </legend>
	<?php
		echo $this->Form->input('amount',array('label' => 'Free Credit Amount'));
		echo $this->Form->input('short_description',array('value' => 'free_credit', 'type'=>'hidden'));
		echo $this->Form->input('service',array('value' => 'free_credit', 'type'=>'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>