<div class="users view">
<h2><?php  echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mobile'); ?></dt>
		<dd>
			<?php echo h($user['User']['mobile']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Enabled'); ?></dt>
		<dd>
			<?php echo h($user['User']['is_enabled']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($user['User']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Priority'); ?></dt>
		<dd>
			<?php echo h($user['User']['priority']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Type'); ?></dt>
		<dd>
			<?php echo h($user['User']['type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['Vehicle']['id'], array('controller' => 'vehicles', 'action' => 'view', $user['Vehicle']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Running Distance'); ?></dt>
		<dd>
			<?php echo h($user['User']['running_distance']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($user['User']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($user['User']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Booking Settings'), array('controller' => 'booking_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Booking Setting'), array('controller' => 'booking_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fare Settings'), array('controller' => 'fare_settings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fare Setting'), array('controller' => 'fare_settings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Notification Logs'), array('controller' => 'notification_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Notification Log'), array('controller' => 'notification_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Queues'), array('controller' => 'queues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Top Ups'), array('controller' => 'top_ups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Top Up'), array('controller' => 'top_ups', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trackings'), array('controller' => 'trackings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tracking'), array('controller' => 'trackings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Trouble Logs'), array('controller' => 'trouble_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Trouble Log'), array('controller' => 'trouble_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Billings'); ?></h3>
	<?php if (!empty($user['Billing'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Promoter Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Billing'] as $billing): ?>
		<tr>
			<td><?php echo $billing['id']; ?></td>
			<td><?php echo $billing['user_id']; ?></td>
			<td><?php echo $billing['promoter_id']; ?></td>
			<td><?php echo $billing['booking_id']; ?></td>
			<td><?php echo $billing['amount']; ?></td>
			<td><?php echo $billing['created']; ?></td>
			<td><?php echo $billing['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'billings', 'action' => 'view', $billing['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'billings', 'action' => 'edit', $billing['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'billings', 'action' => 'delete', $billing['id']), null, __('Are you sure you want to delete # %s?', $billing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Booking Settings'); ?></h3>
	<?php if (!empty($user['BookingSetting'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Min Advance Booking Time'); ?></th>
		<th><?php echo __('Is Advance Booking On'); ?></th>
		<th><?php echo __('Week No'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th><?php echo __('End Date'); ?></th>
		<th><?php echo __('Virtual Rank Ids'); ?></th>
		<th><?php echo __('Is Over Job Notification'); ?></th>
		<th><?php echo __('Over Job Amount'); ?></th>
		<th><?php echo __('Promoter Percentage'); ?></th>
		<th><?php echo __('Repeat Call Duration'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['BookingSetting'] as $bookingSetting): ?>
		<tr>
			<td><?php echo $bookingSetting['id']; ?></td>
			<td><?php echo $bookingSetting['user_id']; ?></td>
			<td><?php echo $bookingSetting['min_advance_booking_time']; ?></td>
			<td><?php echo $bookingSetting['is_advance_booking_on']; ?></td>
			<td><?php echo $bookingSetting['week_no']; ?></td>
			<td><?php echo $bookingSetting['start_date']; ?></td>
			<td><?php echo $bookingSetting['end_date']; ?></td>
			<td><?php echo $bookingSetting['virtual_rank_ids']; ?></td>
			<td><?php echo $bookingSetting['is_over_job_notification']; ?></td>
			<td><?php echo $bookingSetting['over_job_amount']; ?></td>
			<td><?php echo $bookingSetting['promoter_percentage']; ?></td>
			<td><?php echo $bookingSetting['repeat_call_duration']; ?></td>
			<td><?php echo $bookingSetting['created']; ?></td>
			<td><?php echo $bookingSetting['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'booking_settings', 'action' => 'view', $bookingSetting['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'booking_settings', 'action' => 'edit', $bookingSetting['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'booking_settings', 'action' => 'delete', $bookingSetting['id']), null, __('Are you sure you want to delete # %s?', $bookingSetting['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Booking Setting'), array('controller' => 'booking_settings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Fare Settings'); ?></h3>
	<?php if (!empty($user['FareSetting'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Fare For'); ?></th>
		<th><?php echo __('Start Time'); ?></th>
		<th><?php echo __('End Time'); ?></th>
		<th><?php echo __('Start Date'); ?></th>
		<th><?php echo __('End Date'); ?></th>
		<th><?php echo __('Start Lat'); ?></th>
		<th><?php echo __('Start Lng'); ?></th>
		<th><?php echo __('End Lat'); ?></th>
		<th><?php echo __('End Lng'); ?></th>
		<th><?php echo __('Virtual Rank Ids'); ?></th>
		<th><?php echo __('Special Day'); ?></th>
		<th><?php echo __('Fare'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['FareSetting'] as $fareSetting): ?>
		<tr>
			<td><?php echo $fareSetting['id']; ?></td>
			<td><?php echo $fareSetting['user_id']; ?></td>
			<td><?php echo $fareSetting['fare_for']; ?></td>
			<td><?php echo $fareSetting['start_time']; ?></td>
			<td><?php echo $fareSetting['end_time']; ?></td>
			<td><?php echo $fareSetting['start_date']; ?></td>
			<td><?php echo $fareSetting['end_date']; ?></td>
			<td><?php echo $fareSetting['start_lat']; ?></td>
			<td><?php echo $fareSetting['start_lng']; ?></td>
			<td><?php echo $fareSetting['end_lat']; ?></td>
			<td><?php echo $fareSetting['end_lng']; ?></td>
			<td><?php echo $fareSetting['virtual_rank_ids']; ?></td>
			<td><?php echo $fareSetting['special_day']; ?></td>
			<td><?php echo $fareSetting['fare']; ?></td>
			<td><?php echo $fareSetting['created']; ?></td>
			<td><?php echo $fareSetting['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'fare_settings', 'action' => 'view', $fareSetting['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'fare_settings', 'action' => 'edit', $fareSetting['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fare_settings', 'action' => 'delete', $fareSetting['id']), null, __('Are you sure you want to delete # %s?', $fareSetting['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Fare Setting'), array('controller' => 'fare_settings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Notification Logs'); ?></h3>
	<?php if (!empty($user['NotificationLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Purpose'); ?></th>
		<th><?php echo __('Counter'); ?></th>
		<th><?php echo __('Process'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['NotificationLog'] as $notificationLog): ?>
		<tr>
			<td><?php echo $notificationLog['id']; ?></td>
			<td><?php echo $notificationLog['user_id']; ?></td>
			<td><?php echo $notificationLog['purpose']; ?></td>
			<td><?php echo $notificationLog['counter']; ?></td>
			<td><?php echo $notificationLog['process']; ?></td>
			<td><?php echo $notificationLog['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'notification_logs', 'action' => 'view', $notificationLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'notification_logs', 'action' => 'edit', $notificationLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'notification_logs', 'action' => 'delete', $notificationLog['id']), null, __('Are you sure you want to delete # %s?', $notificationLog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Notification Log'), array('controller' => 'notification_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Queues'); ?></h3>
	<?php if (!empty($user['Queue'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Virtual Rank Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Position'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Queue'] as $queue): ?>
		<tr>
			<td><?php echo $queue['id']; ?></td>
			<td><?php echo $queue['virtual_rank_id']; ?></td>
			<td><?php echo $queue['user_id']; ?></td>
			<td><?php echo $queue['position']; ?></td>
			<td><?php echo $queue['created']; ?></td>
			<td><?php echo $queue['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'queues', 'action' => 'view', $queue['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'queues', 'action' => 'edit', $queue['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'queues', 'action' => 'delete', $queue['id']), null, __('Are you sure you want to delete # %s?', $queue['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Queue'), array('controller' => 'queues', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Rejects'); ?></h3>
	<?php if (!empty($user['Reject'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('Rejected Time'); ?></th>
		<th><?php echo __('Rejected By'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Reject'] as $reject): ?>
		<tr>
			<td><?php echo $reject['id']; ?></td>
			<td><?php echo $reject['user_id']; ?></td>
			<td><?php echo $reject['booking_id']; ?></td>
			<td><?php echo $reject['rejected_time']; ?></td>
			<td><?php echo $reject['rejected_by']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rejects', 'action' => 'view', $reject['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rejects', 'action' => 'edit', $reject['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rejects', 'action' => 'delete', $reject['id']), null, __('Are you sure you want to delete # %s?', $reject['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Request Logs'); ?></h3>
	<?php if (!empty($user['RequestLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Booking Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['RequestLog'] as $requestLog): ?>
		<tr>
			<td><?php echo $requestLog['id']; ?></td>
			<td><?php echo $requestLog['booking_id']; ?></td>
			<td><?php echo $requestLog['user_id']; ?></td>
			<td><?php echo $requestLog['created']; ?></td>
			<td><?php echo $requestLog['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'request_logs', 'action' => 'view', $requestLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'request_logs', 'action' => 'edit', $requestLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'request_logs', 'action' => 'delete', $requestLog['id']), null, __('Are you sure you want to delete # %s?', $requestLog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Top Ups'); ?></h3>
	<?php if (!empty($user['TopUp'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Top Up Amount'); ?></th>
		<th><?php echo __('Total Amount'); ?></th>
		<th><?php echo __('Service'); ?></th>
		<th><?php echo __('Last Paid Amount'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['TopUp'] as $topUp): ?>
		<tr>
			<td><?php echo $topUp['id']; ?></td>
			<td><?php echo $topUp['user_id']; ?></td>
			<td><?php echo $topUp['top_up_amount']; ?></td>
			<td><?php echo $topUp['total_amount']; ?></td>
			<td><?php echo $topUp['service']; ?></td>
			<td><?php echo $topUp['last_paid_amount']; ?></td>
			<td><?php echo $topUp['created']; ?></td>
			<td><?php echo $topUp['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'top_ups', 'action' => 'view', $topUp['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'top_ups', 'action' => 'edit', $topUp['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'top_ups', 'action' => 'delete', $topUp['id']), null, __('Are you sure you want to delete # %s?', $topUp['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Top Up'), array('controller' => 'top_ups', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Trackings'); ?></h3>
	<?php if (!empty($user['Tracking'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Lat'); ?></th>
		<th><?php echo __('Lng'); ?></th>
		<th><?php echo __('At Time'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['Tracking'] as $tracking): ?>
		<tr>
			<td><?php echo $tracking['id']; ?></td>
			<td><?php echo $tracking['user_id']; ?></td>
			<td><?php echo $tracking['lat']; ?></td>
			<td><?php echo $tracking['lng']; ?></td>
			<td><?php echo $tracking['at_time']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'trackings', 'action' => 'view', $tracking['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'trackings', 'action' => 'edit', $tracking['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'trackings', 'action' => 'delete', $tracking['id']), null, __('Are you sure you want to delete # %s?', $tracking['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Tracking'), array('controller' => 'trackings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Trouble Logs'); ?></h3>
	<?php if (!empty($user['TroubleLog'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($user['TroubleLog'] as $troubleLog): ?>
		<tr>
			<td><?php echo $troubleLog['id']; ?></td>
			<td><?php echo $troubleLog['user_id']; ?></td>
			<td><?php echo $troubleLog['created']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'trouble_logs', 'action' => 'view', $troubleLog['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'trouble_logs', 'action' => 'edit', $troubleLog['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'trouble_logs', 'action' => 'delete', $troubleLog['id']), null, __('Are you sure you want to delete # %s?', $troubleLog['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Trouble Log'), array('controller' => 'trouble_logs', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
