<style>
	form div.checkbox{
		float: left;
		clear: none;
		width: 300px;
	}
</style>
<?php 
	echo $this->Html->css(array('jquery-ui', 'style-ui'));
	echo $this->Html->script(array('jquery-ui')); 
?>
<script type="text/javascript">
	$(function(){
		$('#insurance_certificate').datepicker({dateFormat : 'dd/mm/yy'});
	});

	// autocomplete bug fix
	$(window).load(function() {
		setTimeout(function () {
			$('input#UserPasswordx').val('').css('background-color', '#FFFFFF !important');
	       	$('input#UserName').val($('input#UserName').data('value')).css('background-color', '#FFFFFF !important');
   		}, 1000);
	}); 
</script> 
<?php 
echo $this->Html->script(array('image_hover')); ?>
<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('data-value' => $this->request->data['User']['name']));
		echo $this->Form->input('email');
		echo $this->Form->input('address');
		echo $this->Form->input('mobile');
		if($this->request->data['User']['type'] == 'driver') { 
			echo $this->Form->input('voip_no', array('label' => 'Phone Number for Answering Passengers Phone Calls (VOIP No)'));
		}
		echo $this->Form->input('home_no');
		echo $this->Form->input('passwordx', array('label' => 'Password','type' => 'Password', 'value' => ''));
		//echo $this->Form->input('is_enabled');
		echo $this->Form->input('code', array('type' => 'hidden'));
		// echo $this->Form->input('priority');
		echo $this->Form->input('type', array('type' => 'hidden'));
	if($this->request->data['User']['type'] == 'driver') { ?>
		<?php echo $this->Form->input('account_holder_name', array('label' => "Account Holder's Name")); 
		echo $this->Form->input('account_no'); 
		echo $this->Form->input('sort_code'); 
		echo $this->Form->input('iban', array('label' => 'IBAN')); 
		echo $this->Form->input('swift', array('label' => 'SWIFT/BIC code')); 
		?>
		<p><font face='verdana' size=3>Your Image</font></p>
		<?php if(empty($this->request->data['User']['image'])) { ?>
			<i>(No Image Uploaded)</i>
		<?php }else {?>
		<div class='thumbnail-item'>
		<?php echo $this->Html->image('/files/driver_infos/small/' . $this->request->data['User']['image'],array('alt'=>'your image', 'class'=> 'thumbnail')); 
		?>
			<div class='tooltip'>
				<?php
				echo $this->Html->image('/files/driver_infos/' . $this->request->data['User']['image'],array('width'=> '500','height' => '300'));
				?> 
			</div>
		</div>
		<?php }
		echo $this->Form->input('imagex',array('type' =>'file', 'label' => false));
		echo $this->Form->input('badge_no');
		?><p><font face='verdana' size=3>Image of Driver Badge</font></p>
		<?php if(empty($this->request->data['User']['badge_image'])) { ?>
			<i>(No Image Uploaded)</i>
		<?php }else {?>
		<div class='thumbnail-item'><?php
		echo $this->Html->image('/files/driver_infos/small/' . $this->request->data['User']['badge_image'],array('alt'=>'image of driver badge','class'=> 'thumbnail')); 
		?>
			<div class='tooltip'>
				<?php
				echo $this->Html->image('/files/driver_infos/' . $this->request->data['User']['badge_image'],array('width'=> '500','height' => '300'));
				?> 
			</div>
		</div>
		<?php }
		echo $this->Form->input('badge_imagex',array('type' =>'file', 'label' => false));
		//print_r($this->request->data['User']['cab_type']); 
		echo $this->Form->input('cab_type', array('options' => array('Private Hire' => 'Private Hire', 'Hackney Carriage' => 'Hackney Carriage'), 'default' => $this->request->data['User']['cab_type'], 'empty' => '(Select One)'));
		echo $this->Form->input('vehicle_type', array('label' => 'Car Type', 'options' => array('Saloon' => 'Saloon', 'Estate' => 'Estate'), 'default' => $this->request->data['User']['vehicle_type'], 'empty' => '(Select One)'));
		echo $this->Form->input('registration_plate_no');
		echo $this->Form->input('no_of_seat', array('options' => array('2' => '2 Seats', '4' => '4 Seats', '5' => '5 Seats', '6' => '6 Seats', '7' => '7 Seats', '8' => '8 Seats'), 'empty' => '(Select One)'));
		echo $this->Form->input('is_wheelchair', array('label' => 'Wheelchair Access?'));
		echo $this->Form->input('vehicle_licence_no');
		?><p><font face='verdana' size=3>Vehicle Licence Image</font></p>
		<?php if(empty($this->request->data['User']['vehicle_licence_image'])) { ?>
			<i>(No Image Uploaded)</i>
		<?php }else {?>
		<div class='thumbnail-item'><?php
		echo $this->Html->image('/files/driver_infos/small/' . $this->request->data['User']['vehicle_licence_image'],array('alt'=>'vehicle licence image','class'=> 'thumbnail')); 
		?>
			<div class='tooltip'>
				<?php
				echo $this->Html->image('/files/driver_infos/' . $this->request->data['User']['vehicle_licence_image'],array('width'=> '500','height' => '300'));
				?> 
			</div>
		</div>
		<?php }
		echo $this->Form->input('vehicle_licence_imagex',array('type' =>'file', 'label' => false));
		if($this->request->data['User']['insurance_certificate'] == '0000-00-00') {
			//$now = date_format('d/m/Y');
			//echo $now;
			echo $this->Form->input('insurance_certificate',array('label' => 'Insurance Date Of Expiry', 'id' => 'insurance_certificate','value' => date('d/m/Y'),'style' => 'width:175px; height:30px; padding:0','type' => 'text'));

		} else {
			echo $this->Form->input('insurance_certificate',array('label' => 'Insurance Date Of Expiry', 'id' => 'insurance_certificate','value' => date_format(date_create($this->request->data['User']['insurance_certificate']), 'd/m/Y'),'style' => 'width:175px; height:30px; padding:0','type' => 'text'));
		}
		?><p><font face='verdana' size=3>Insurance Certificate Image</font></p>
		<?php if(empty($this->request->data['User']['insurance_certificate_image'])) { ?>
			<i>(No Image Uploaded)</i>
		<?php }else {?>
		<div class='thumbnail-item'><?php
		echo $this->Html->image('/files/driver_infos/small/' . $this->request->data['User']['insurance_certificate_image'],array('alt'=>'insurance certificate image','class'=> 'thumbnail')); 
		?>
			<div class='tooltip'>
				<?php
				echo $this->Html->image('/files/driver_infos/' . $this->request->data['User']['insurance_certificate_image'],array('width'=> '500','height' => '300'));
				?> 
			</div>
		</div>
		<?php }
		echo $this->Form->input('insurance_certificate_imagex',array('type' =>'file', 'label' => false));
		echo $this->Form->input('running_distance', array('label' => 'Running Distance (miles)'));
		echo $this->Form->input('promoter_percentage',array('label' => 'Override Promoter Percentage %'));
		echo $this->Form->input('admin_percentage',array('label' => 'Override Admin Percentage %'));
		echo $this->Form->input('admin_percentage_cash',array('label' => 'Override Admin Percentage % For Cash Payment'));
		echo $this->Form->input('Zone', array('hiddenField' => false, 'label' => '<strong>VR Zone (If ticked NOT allowed)</strong>','type' => 'select', 'multiple' => 'checkbox', 'options' => $vr_zones));
		echo $this->Form->input('Zone', array('hiddenField' => false, 'label' => '<strong>Phone GPS Zone (If ticked allowed)</strong>','type' => 'select', 'multiple' => 'checkbox', 'options' => $phonecall_gps_zones));
		echo $this->Form->input('phonecall_zone', array('hiddenField' => false, 'label' => '<strong>Phonecall Zone (If selected allowed)</strong>','type' => 'select','options' => $phonecall_zones, 'empty' => '(Select One)'));
	}
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>