<div class="settings form">
<?php echo $this->Form->create('PhoneSetting'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Phonecall and Phone GPS Zones Settings'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('zone_id', array('options' => $zones, 'label' => 'Phonecall and Phone GPS Zones'));
		echo $this->Form->input('req_timeout', array('label' => 'Try Each Logged In Driver for Number of Seconds'));
		echo $this->Form->input('remain_loggedin_on_accept', array('label' => 'After Accepting a Job Leave Drivers Logged IN'));
		echo $this->Form->input('reject_threshold', array('label' => 'Log Drivers OUT after number of Ignore or Reject Jobs'));
		echo $this->Form->input('allow_inactive_time', array('label' => 'Allowed Inactive Time'));
		echo $this->Form->input('setting_type',array('type'=>'hidden'));
		echo $this->Form->input('min_call_duration', array('label' => 'Ignore Calls Shorter then Numer of Seconds'));
		echo $this->Form->input('minimum_charge', array('label' => 'Minimum Charge'));
		echo $this->Form->input('per_minute_charge', array('label' => 'Per Minute Charge'));
		echo $this->Form->input('call_cut_off_time', array('label' => 'Cut Calls off If Answered Within Milliseconds of Connection'));
		echo $this->Form->input('repeat_call_timeout', array('label' => 'Route Repeat Calls to the Same Driver Within'));
		echo $this->Form->input('bar_private_no', array('label' => 'Bar all Incoming Private & Withheld Numbers'));
		?><strong>Try Alternate Zones When No One is Available to Take Calls</strong>

		<?php
		if($this->request->data['PhoneSetting']['restriction'] != null) {
			$zn = trim($this->request->data['PhoneSetting']['restriction'],'#');
            $zns = explode('#', $zn); 
            //print_r($zns);
            $i = 0;
            foreach ($zones as $index => $zone) {
				echo $this->Form->input('restriction', array('hiddenField' => false, 'name' => 'data[PhoneSetting][restriction][]', 'label' => false,'type' => 'select', 'selected' => $zns[$i++], 'options' => $zones, 'empty' => '(No zone)'));
			}
        } else { 
            echo $this->Form->input('restriction', array('hiddenField' => false, 'name' => 'data[PhoneSetting][restriction][]', 'label' => false,'type' => 'select', 'options' => $zones, 'empty' => '(No zone)'));
        } 
		echo $this->Form->input('divert_phoneno', array('label' => 'Divert the call'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element('menu'); ?>