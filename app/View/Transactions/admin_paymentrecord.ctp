<div class="transaction index">
	<h2><?php echo __('Admin Fee Records'); ?></h2>
	<h3>Total Amount <?php echo $this->Number->currency(trim($sum_amount[0][0]['total'],'-'), 'GBP');?></h3>

	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('created', 'Transaction Date'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id', "Driver's Name"); ?></th>
			<th><?php echo $this->Paginator->sort('amount'); ?></th>
			<th><?php echo $this->Paginator->sort('booking_id', 'Booking Id'); ?></th>
	</tr>
	<?php // pr($sum_amount); ?>
	<?php foreach ($records as $record): ?>
	<tr>
		<td>
			<?php $created = date_create($record['Transaction']['created']);
			 echo date_format($created, 'd/m/Y g:i A'); ?>
		</td>
		<td>
			<?php echo $this->Html->link($record['User']['name'], array('controller' => 'users','action' => 'edit', $record['User']['id'])); ?>
		</td>
		<td><?php
		$fee = trim($record['Transaction']['amount'], '-');
		echo $this->Number->currency($fee, 'GBP'); 
		?>&nbsp;
		</td>
			<td>
			<?php echo $this->Html->link($record['Transaction']['booking_id'], array('controller' => 'bookings','action' => 'view', $record['Transaction']['booking_id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element('menu'); ?>