<?php echo $this->Html->script('booking');?>
<div class="bookings form">
<?php echo $this->Form->create('Booking'); ?>
	<fieldset>
		<legend><?php echo __('Add Booking'); ?></legend>
	<?php
		echo $this->Form->input('pick_up');
		//echo $this->Form->input('pick_up_lat');
		//echo $this->Form->input('pick_up_lng');
		echo $this->Form->input('destination');
		//echo $this->Form->input('destination_lat');
		//echo $this->Form->input('destination_lng');
		echo $this->Form->input('at_time');
		echo $this->Form->input('persons', array('label' => 'No of Passengers', 'options' => array('1'=> '1 Passengers', '2' => '2 Passengers', '3' => '3 Passengers', '4' => '4 Passengers', '5' => '5 Passengers', '6' => '6 Passengers', '7' => '7 Passengers', '8'=> '8 Passengers')));
		echo $this->Form->input('car_type', array('options' => array('ANY', 'Saloon car', '5 Seats', '6 Seats', '7 Seats', '8 Seats','Estate', 'Wheelchair')));
		echo $this->Form->input('no_of_luggages', array('options' => array('1','2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20')));
		//echo $this->Form->input('recurring', array('label' => 'Repeat Booking', 'options' => array('Once', 'Daily', 'Weekly', 'Every weekday', 'Monthly')));
		//echo $this->Form->input('recurring_end', array('label' => 'Repeat End'));
		//echo $this->Form->input('acceptor');
		//echo $this->Form->input('accept_time');
		//echo $this->Form->input('special_requirement');
		//echo $this->Form->input('bar_driver');
		echo $this->Form->input('note_to_driver');
		//echo $this->Form->input('is_app_to_app');
		//echo $this->Form->input('booking_type');
		//echo $this->Form->input('is_sap_booking', array('label' => 'SAP Booking?'));

		echo $this->Form->input('from_web', array('value' => '1', 'type' => 'hidden'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
