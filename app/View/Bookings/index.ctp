<div class="bookings index">
	<h2><?php echo __('Bookings'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_lat'); ?></th>
			<th><?php echo $this->Paginator->sort('pick_up_lng'); ?></th>
			<th><?php echo $this->Paginator->sort('destination'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_lat'); ?></th>
			<th><?php echo $this->Paginator->sort('destination_lng'); ?></th>
			<th><?php echo $this->Paginator->sort('at_time'); ?></th>
			<th><?php echo $this->Paginator->sort('persons'); ?></th>
			<th><?php echo $this->Paginator->sort('via'); ?></th>
			<th><?php echo $this->Paginator->sort('fare'); ?></th>
			<th><?php echo $this->Paginator->sort('is_paid'); ?></th>
			<th><?php echo $this->Paginator->sort('requester'); ?></th>
			<th><?php echo $this->Paginator->sort('acceptor'); ?></th>
			<th><?php echo $this->Paginator->sort('accept_time'); ?></th>
			<th><?php echo $this->Paginator->sort('special_requirement'); ?></th>
			<th><?php echo $this->Paginator->sort('bar_driver'); ?></th>
			<th><?php echo $this->Paginator->sort('note_to_driver'); ?></th>
			<th><?php echo $this->Paginator->sort('is_app_to_app'); ?></th>
			<th><?php echo $this->Paginator->sort('is_vr'); ?></th>
			<th><?php echo $this->Paginator->sort('is_pre_book'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($bookings as $booking): ?>
	<tr>
		<td><?php echo h($booking['Booking']['id']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['pick_up']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['pick_up_lat']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['pick_up_lng']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['destination']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['destination_lat']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['destination_lng']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['at_time']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['persons']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['via']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['fare']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['is_paid']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['requester']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['acceptor']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['accept_time']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['special_requirement']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['bar_driver']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['note_to_driver']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['is_app_to_app']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['is_vr']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['is_pre_book']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['created']); ?>&nbsp;</td>
		<td><?php echo h($booking['Booking']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $booking['Booking']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $booking['Booking']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $booking['Booking']['id']), null, __('Are you sure you want to delete # %s?', $booking['Booking']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Booking'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Billings'), array('controller' => 'billings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Billing'), array('controller' => 'billings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Rejects'), array('controller' => 'rejects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Reject'), array('controller' => 'rejects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Request Logs'), array('controller' => 'request_logs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Request Log'), array('controller' => 'request_logs', 'action' => 'add')); ?> </li>
	</ul>
</div>
