<?php 
		$sts = array('no_driver_found' => 'No Driver Found/Responsed', 'cancelled_by_passenger' => 'Cancelled By Passenger', 'accepted' => 'Accepted','timeout'=> 'Timeout', 'cleared' => 'Cleared', 'rejected' => 'Rejected','forwarded' => 'Forwarded', 'ignored' => 'Timeout','timeout'=> 'Timeout', 'request_sent' => 'Request Sent'); 
	?>
<div class="bookings view">
<h2><?php  echo __('Booking Id '); echo h($booking['Booking']['id']); ?></h2>
	<dl>
		<dt><?php echo __('Pick Up'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['pick_up']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Destination'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['destination']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('At Time'); ?></dt>
		<dd>
			<?php  
			if (!empty($booking['Booking']['at_time'])){
				$date_time = date_create($booking['Booking']['at_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
			?>&nbsp;
		</dd>
		<dt><?php echo __('SAP Booking'); ?></dt>
		<dd>
			<?php if(($booking['Booking']['is_sap_booking']) == '1') echo 'Yes'; 
			else 'No';
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Passengers'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['persons']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fare'); ?></dt>
		<dd>
			<?php echo $this->Number->currency($booking['Booking']['fare'], 'GBP'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Real Fare'); ?></dt>
		<dd>
			<?php echo $this->Number->currency($booking['Booking']['real_fare'], 'GBP'); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Requester'); ?></dt>
		<dd>
			<?php echo $this->Html->link($booking['Requester']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['requester'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Acceptor'); ?></dt>
		<dd>
			<?php 
				if(!empty($booking['Booking']['acceptor'])) {
					echo $this->Html->link($booking['Acceptor']['name'], array('controller' => 'users', 'action' => 'edit', $booking['Booking']['acceptor'])); 
				} else {
					if(empty($booking['Booking']['status'])) echo 'Queued';
					elseif ($booking['Booking']['status'] =='no_driver_found' AND $booking['Booking']['is_sap_booking'] == '1' AND $booking['Booking']['from_web']!='1') {
						echo 'Cancelled';
					}elseif($booking['Booking']['status'] =='no_driver_found' AND $booking['Booking']['is_sap_booking'] == '1' AND $booking['Booking']['from_web']=='1') {
						echo 'No Driver Found';
					}else {
						$val = $booking['Booking']['status'];
						echo $sts[$val]; 
					}
				}
			?>&nbsp;

		</dd>
		<?php if(!empty($booking['Booking']['hunting_no'])) { ?>
		<dt><?php echo __('Inquery No'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['hunting_no']); ?>&nbsp;
		</dd>
		<?php } ?>
		<dt><?php echo __('Accept Time'); ?></dt>
		<dd>
			<?php 
			if (!empty($booking['Booking']['accept_time'])){
				$date_time = date_create($booking['Booking']['accept_time']);
				echo date_format($date_time,'d/m/Y g:i A');
			} else {
				echo 'N/A';
			}
			?>&nbsp;
		</dd>
		<dt><?php echo __('Advanced Booking'); ?></dt>
		<dd>
			<?php 
			if ($booking['Booking']['is_pre_book'] == '1'){
				echo 'Yes';
			} else {
				echo 'No';
			}
			?>&nbsp;
		</dd>
		<dt><?php echo __('Special Requirement'); ?></dt>
		<dd>
			<?php 
			if (!empty($booking['Booking']['special_requirement'])){
				echo h($booking['Booking']['special_requirement']); 
			} else {
				echo 'N/A';
			}
			?>
			&nbsp;
		</dd>
		<dt><?php echo __('Note To Driver'); ?></dt>
		<dd>
			<?php echo h($booking['Booking']['note_to_driver']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php 
			$date_time = date_create($booking['Booking']['created']);
			echo date_format($date_time,'d/m/Y g:i A');
			?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php echo $this->element('menu'); ?>