$(function(){
	$('#AddMore').on('click', 
		function(e){
			e.preventDefault();
			$('div.AddMoreClass:last').clone().insertBefore($(this).parent());
			$('.fareClass:last').text('Per Mile Fare');
			$('div.AddMoreClass:last input').val('');
		});
});