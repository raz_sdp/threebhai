$(function() {
    $('#VrSettingHolidaytypeId').on('change',
    function() { 
        if (parseInt($(this).val()) > 0) {
            $('.days').hide();
            $('.input.date').hide();
        } else {
            $('.days').show();
            $('.input.date').show();
        }
    }).change();
    $("#VrSettingIsAdvanceBookingOn").on('click',
    function() {
    	console.log($(this).prop('checked'));
        if ($(this).prop('checked')) {
            $('.isenablebooking').show();
        } else {
            $('.isenablebooking').hide();
        }
    });
    $("#VrSettingIsOverJobNotification").on('click',
    function() {
        if ($(this).prop('checked')) {
            $('.isenable').show();
        } else {
            $('.isenable').hide();
        }
    });
});