<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	 public $components = array(
        'Session', 'Cookie', 'RequestHandler',
        'Auth' => array(
			'loginAction' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
            'loginRedirect' => array('controller' => 'users', 'action' => 'index', 'admin' => true),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login', 'admin' => true),
			'authError' => 'You are not allowed',
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email', 'password' => 'password')
				)
			)
        )
    );


    public function beforeFilter() {
        if($this->params->prefix== 'admin' && $this->Auth->User('type')!='admin') $this->Auth->logout();
        //print_r($this->params->url);

        if($this->params['admin']){
            $controller = $this->params['controller'];
            $action = $this->params['action'];
            if($controller=='users' && $action=='admin_despatch_job') {
                $this->layout = 'default';
            } else {
                $this->layout = 'admin';
            }

        }
        elseif ($this->params->url == 'pages/login') {
            $this->layout = 'login';
        }

    	//for get request
    	if(!empty($this->params->pass[0])){
			if (preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/", $this->params->pass[0])) {
			  	$code = $this->params->pass[0];
			} 
    	}
    	//for post rrequest
    	if(!empty($this->params->data[$this->modelClass]['code'])){
			if (preg_match("/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/", $this->params->data[$this->modelClass]['code'])) {
			 	$code = $this->params->data[$this->modelClass]['code'];
                $this->loadModel('User');
                $this->User->recursive = -1;
                $userExist = $this->User->findByCode($code, array('User.id'));
                if(!empty($userExist)){
                    $this->User->id = $userExist['User']['id'];
                    $this->User->saveField('last_activity', date('Y-m-d H:i:s'));           
                }
			} 
    	}
    	
        $this->Auth->allow(
           'display','register', 'update', 'iforgot', 'pwreset', 'booking_settings', 'distributedBooking','driver_forward', 'get_vr_setting',
            'accept', 'booking', 'g', 'zonelists', 'utrac', 'gtrac', 'cncl', 'hunt', 'sdvr', 'pndg', 'gbard', 'dobar', 'login', 'logout',
            'undobar', 'isdav', 'spsngrs','gdqp', 'push', 'user_logout', 'ondr','pobtm','nshwt', 'jclr', 'do_top_up', 'cancel_sap_booking','driver_in_danger_info',
			'emergency','interrupt', 'udinfo', 'do_trans', 'guab', 'vun', 'position', 'cash_in_hand', 'cronjobZoneSettings','bank_details',
            'passenger_login','driver_login','profit', 'verify_payment', 'credit_card_pay_verification', 'do_pay', 'due_admin','twilio_call',
            'twilio_complete','twilio_screen', 'req_fr_promoter', 'req_transfer_amount','billing_twillio','cronjobRejectThreshold', 'try_driver_again',
            'book_a_taxi', 'proxy', 'sendto_passenger', 'update_device_token','paypal_info', 'is_updated_info', 'info', 'cleanup', 'email_send_cronjob', 'test', 'balance_check', 'test_push',
            'login','search_call', 'recurrence_billing', 'rent_transaction', 'sufficient_balance', 'reminder_rent_fee', 'low_balance_email', 'driver_invoice',
            'view_pdf', 'ipn', 'success', 'cancel',
            'vendor_registration', 'distributor_registration', 'branchdisplay', 'near_by_drivers', 'validate_input','despatch_job', 'around_you'
        );
        if (!defined('CURRENCY')){
            //$TEST = array('currency_code' => 'GBP', 'free_credit' => 'Free Credit');
            define('CURRENCY', 'GBP');
        }
    }

    // api log
    function _apiLog($user_id, $payload, $response, $status, $time_pre){
        $this->loadModel('Apilog');
        $time_post = microtime(true);
        $data = array(
          'Apilog' => array(
            'request_url' => $this->params->url,
            'user_id' => $user_id,
            'payload' => $payload,
            'response' => $response,
            'ip_address' => $_SERVER['REMOTE_ADDR'],
            'status' => $status,
            'time' => round(($time_post - $time_pre), 4)
          )
        );
        // $this->Apilog->create();
        // $this->Apilog->save($data,false);

        $delete_apilogs = $this->Apilog->query('
            DELETE FROM apilogs WHERE DATE(created) < DATE_SUB(CURDATE(),INTERVAL 4 DAY)');
    }

    //entry admin percentage for a booking 
    function _fee_admin_entry($user_info = null, $fare = null, $booking_id = null, $service = null) {
        
        $this->loadModel('Setting');
        if($service != 'cash_in_hand') {
            $admin_percentage = $user_info['User']['admin_percentage'];
            if (empty($admin_percentage)) {     
                $admin_percentage_settings = $this->Setting->findById(1, array('admin_percentage'));
                $admin_percentage = $admin_percentage_settings['Setting']['admin_percentage'];
            }
        } else {
            $admin_percentage = $user_info['User']['admin_percentage_cash'];
            if (empty($admin_percentage)) {     
                $admin_percentage_settings = $this->Setting->findById(1, array('admin_percentage_cash'));
                $admin_percentage = $admin_percentage_settings['Setting']['admin_percentage_cash'];
            }
        }
        $admin_amount =  $this->User->Transaction->getAdminEarning($admin_percentage, $fare);
        $data['Transaction']['user_id'] = $user_info['User']['id'];
        $data['Transaction']['amount'] = '-'.$admin_amount;
        $data['Transaction']['short_description'] = 'admin_fee';
        $data['Transaction']['referred_user_id'] = null;
        $now = date('Y-m-d');
        $data['Transaction']['timestamp'] = $now;
        $data['Transaction']['booking_id'] = $booking_id;
        $data['Transaction']['currency_code'] = CURRENCY;
        $data['Transaction']['service'] = 'admin_payment';
        $this->Transaction->create();
        $this->Transaction->save($data, false);
        /*if($this->Transaction->save($data, false)){
            $a_data['Transaction']['user_id'] = $user_info['User']['id'];
            $a_data['Transaction']['amount'] = '-'.$admin_amount;
            $a_data['Transaction']['short_description'] = 'admin_fee';
            $a_data['Transaction']['referred_user_id'] = null;
            $now = date('Y-m-d');
            $a_data['Transaction']['timestamp'] = $now;
            $a_data['Transaction']['booking_id'] = $booking_id;
            $a_data['Transaction']['currency_code'] = CURRENCY;
            $a_data['Transaction']['service'] = 'admin_payment';
            $this->Transaction->create();
            $this->Transaction->save($a_data, false);
        }*/
    }

    // promoter percentage calculation and give the promoter calculated credit 
    function _pay_promoter($promoter_info = null, $user_id = null, $amount = null){
    
        $promoter_percentage = $promoter_info['User']['promoter_percentage'];
        if (empty($promoter_percentage)) {
            $this->loadModel('Setting');
            $promoter_percentage = $this->Setting->findById(1,array('promoter_percentage'));
            $promoter_amount = ($amount * $promoter_percentage['Setting']['promoter_percentage']) /100;
        } else {
            $promoter_amount = ($amount * $promoter_percentage) /100;   
        }
        $driver['Transaction']['user_id'] = $user_id;
        $driver['Transaction']['amount'] = '-'.$promoter_amount;
        $driver['Transaction']['payee'] = $promoter_info['User']['id'];
        $driver['Transaction']['short_description'] = 'promoter_fee';
        $now = date('Y-m-d');
        $driver['Transaction']['timestamp'] = $now;
        $driver['Transaction']['service'] = 'promoter_fee';
        $driver['Transaction']['currency_code'] = CURRENCY;
        $this->Transaction->create();
        if($this->Transaction->save($driver, false)) {
            $data['Transaction']['user_id'] = $promoter_info['User']['id'];
            $data['Transaction']['amount'] = $promoter_amount;
            $data['Transaction']['short_description'] = 'promoter_profit';
            $data['Transaction']['referred_user_id'] = $user_id;
            $now = date('Y-m-d');
            $data['Transaction']['timestamp'] = $now;
            $data['Transaction']['service'] = 'promoter_profit';
            $data['Transaction']['currency_code'] = CURRENCY;
            $this->Transaction->create();
            $this->Transaction->save($data, false);
        }
    }

    function _queue($driver, $booking_id, $zone_id){
        $data = array();
        $this->recursive = -1;
        $data['Queue']['user_id'] = $driver['Driver']['user_id'];
        $data['Queue']['booking_id'] = $booking_id;
        $now = date("Y-m-d H:i:s");
        $data['Queue']['request_time'] = $now;
        $data['Queue']['zone_id'] = $zone_id;
        //$Queue = new Queue();
        $this->loadModel('Booking');
        $booking_details = $this->Booking->findById($booking_id, array('is_sap_booking', 'pick_up', 'persons'));
        $is_sap_booking = $booking_details['Booking']['is_sap_booking'];
        
        // FIXME: check if the driver alreay got that job request
        $check_driver = $this->Queue->findByBookingIdAndUserId($booking_id, $driver['Driver']['user_id'], array('Queue.id'));
        if(!empty($check_driver)) return false;
        
        // FIXME: check if the booking has already been accepted, cleared or auto_cancelled or the request has been sent to other driver
       /* $check_response = $this->Queue->find('first', array(
                'conditions'=>array(
                        'Queue.response' => array('accepted', 'cleared','auto_cancelled'),
                        'Queue.response IS NULL',
                        'Queue.booking_id' => $booking_id
                    ),
                'fields' => array('Queue.id'),
            ));
        */
        if(!empty($check_response)) return false;
        $pick_up = (strlen($booking_details['Booking']['pick_up']) > 50) ? substr($booking_details['Booking']['pick_up'],0,50).'...' : $booking_details['Booking']['pick_up'];
        $passenger = $booking_details['Booking']['persons'];
        if($is_sap_booking == 1) {
            $this->Booking->id = $booking_id;
            $this->Booking->saveField('status', 'request_sent');
        }
        $this->Queue->create();

        $this->Queue->save($data);

        // send notification to driver
        $this->_push($driver['Driver']['user_id'], 'You\'ve got a new booking.','CabbieCall', $booking_id, null, null, null, $pick_up, $passenger);

        $driver_zone = $driver['Driver']['vr_zone'];

        $this->_position_driver($driver_zone, $driver['Driver']['user_id']);
        
    
    }

    function _search_driver($booking_id, $lat, $lng, $requester, $passenger, $car, $is_sap_booking = -1){
        $this->loadModel('Booking');
        $booking = $this->Booking->findById($booking_id, array('Booking.zone_id', 'Booking.status', 'Booking.from_web'));
        $zone_id = $booking['Booking']['zone_id'];

        $driver = $this->_driver($lat, $lng, $zone_id, $booking_id, $requester, $passenger, $car);

        $srch_dvr = null;
          
        if(!empty($driver)) {
            $this->_queue($driver, $booking_id, $zone_id);
            if($is_sap_booking == 1) {

                $srch_dvr['search_driver'] = true;
                return $srch_dvr;
            }
        } else {
            //inform passenger that no driver found.
            
                $nearest_phone_no = $this->Queue->query("SELECT
                        g.id, g.phone, g.address,
                            3963.0 * ACOS(
                                SIN(b.pick_up_lat * PI() / 180)* SIN(g.lat * PI() / 180)+ COS(b.pick_up_lat * PI() / 180)* COS(g.lat * PI() / 180)* COS(
                                    (b.pick_up_lng * PI() / 180)-(g.lng * PI() / 180)
                                )
                            )AS distance
                        FROM
                            geophonebooks AS g
                        LEFT JOIN bookings b ON b.id = '$booking_id'
                        ORDER BY
                            distance ASC
                        LIMIT 1");
                $phone = $nearest_phone_no[0]['g']['phone'];
                $requester = $this->Queue->Booking->find('first', array(
                        'recursive' => -1,
                        'joins' => array(
                            array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type' => 'LEFT',
                            'conditions' => 'User.id = Booking.requester'
                            ),
                        ),
                        'conditions' => array(
                            'Booking.id' => $booking_id
                        ),
                        'fields' => array(
                            'User.id', 'User.type','User.email','User.mobile', 'User.name'
                        )
                    )
                );
                if(trim($requester['User']['type']) == 'passenger') {
                    if($booking['Booking']['status'] != 'no_driver_found') {

                        if($booking['Booking']['from_web'] == 1){
                            try{
                                $this->_sendSms($requester['User']['mobile'], 'CabbieAppUK! - No Drivers are available, please wait or call '. $phone); 
                                $this->_sendEmail($requester['User']['email'], "CabbieAppUK! - Booking Confirmation ". $booking_id, "Dear ".$requester['User']['name'].",<br/><br/>There are no drivers available right now, if you would like to call to change or cancel you booking please call ". $phone. ".<br/><br/>Regards,<br/><br/>The CabbieApp Team"); 
                            } catch(Exception $e){

                            }
                            
                        }

                        $driver_not_found_bookings =  $this->Session->read('driver_not_found_bookings');

                        $driver_not_found_bookings[$requester['User']['id']] = array(
                           'requester' => $requester['User']['id'], 
                            'msg' => 'No Drivers are available, please wait or call '. $phone .". For Detail Go 'Open Bookings'", 
                            'app' => 'CabbieApp', 
                            'booking_id' => $booking_id, 
                            'phone' => $phone
                        );
                        $this->Session->write('driver_not_found_bookings', $driver_not_found_bookings);

                    }  

                }
                $this->Queue->Booking->query("
                    UPDATE bookings SET status = 'no_driver_found', hunting_no = '$phone' 
                    WHERE id = '$booking_id'
                "); 

               if($is_sap_booking == 1) {
               
                $srch_dvr['phone'] = $phone;
                $srch_dvr['search_driver'] = false;
               // print_r($srch_dvr);
                return $srch_dvr;
            } 
        }
            
    }

    function _driver($lat, $lng, $zone_id = null, $booking_id, $requester, $passenger, $car){
        $driver = null;
        $car_passenger_cond = '';
        $car_passenger_cond_alt = '';
        switch ($car) {
            case '5 Seats':
                $car_passenger_cond = " AND u.no_of_seat >= '5' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '5' ";
                break;
            case '6 Seats':
                $car_passenger_cond = " AND u.no_of_seat >= '6' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '6' ";
                break;
            case '7 Seats':
                $car_passenger_cond = " AND u.no_of_seat >= '7' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '7' ";
                break;
            case '8 Seats':
                $car_passenger_cond = " AND u.no_of_seat >= '8' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '8' ";
                break;
            case 'Estate':
                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Estate' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Estate' ";
                break;
            case 'Wheelchair':
                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.is_wheelchair = '1' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.is_wheelchair = '1' ";
                break;
            case 'ANY':
                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' ";
                break;
            case 'Saloon car':
                $car_passenger_cond = " AND u.no_of_seat >= '$passenger' AND u.vehicle_type = 'Saloon' ";
                $car_passenger_cond_alt = " AND User.no_of_seat >= '$passenger' AND User.vehicle_type = 'Saloon' ";
                break;
            default:
                $car_passenger_cond = " ";
                $car_passenger_cond_alt = " ";
                break;
        }
        $this->loadModel('Setting');
        $this->loadModel('Queue');
        $driver_min_balance = $this->Setting->findById(1, array('driver_min_balance'));
        $min_balance = $driver_min_balance['Setting']['driver_min_balance'];
        if (!empty($zone_id)) {
            $driver = $this->Queue->query("SELECT
                Driver.user_id as user_id,
                Driver.request_time as request_time,
                Driver.response as response,
                Driver.lat as lat,
                Driver.lng as lng,
                Driver.vr_zone as vr_zone
                    FROM
                        (
                            SELECT
                                q.user_id,
                                q.request_time,
                                q.response,
                                u.lat,
                                u.lng,
                                u.vr_zone
                            FROM
                                queues q
                            INNER JOIN users u ON(
                                u.id = q.user_id
                                AND u.vr_available = 'yes'
                                AND u.vr_zone = '$zone_id'
                                AND u.is_enabled = '1'
                                AND u.type = 'driver'
                                AND u.id <> '$requester'
                                AND(
                                    (
                                        u.running_distance IS NOT NULL
                                        AND 3963.0 * ACOS(
                                                SIN('$lat' * PI() / 180)* SIN(u.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(u.lat * PI() / 180)* COS(
                                                    ('$lng' * PI() / 180)-(u.lng * PI() / 180)
                                                )
                                            ) <= u.running_distance
                                    )
                                    OR(
                                        u.running_distance IS NULL
                                    )
                                )
                            )
                            WHERE q.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id = '$booking_id'
                                UNION ALL
                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
                                UNION ALL
                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = q.user_id AND userId2 = '$requester'
                                UNION ALL
                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 =q.user_id) 
                            AND (SELECT SUM(amount) FROM transactions WHERE user_id = q.user_id 
                                        AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'
                            $car_passenger_cond
                            ORDER BY
                                q.request_time DESC
                        )Driver
                    GROUP BY
                        Driver.user_id
                    ORDER BY
                        Driver.request_time ASC
                    LIMIT 1
                ");
        }
        // search for nearest driver when there is no one avaliable in the zone
        if(empty($driver)){
            $driver = $this->Queue->query("SELECT  
            Driver.user_id as user_id,
            Driver.request_time as request_time,
            Driver.response as response,
            Driver.lat as lat,
            Driver.lng as lng,
            Driver.vr_zone as vr_zone
                FROM
                    (
                    SELECT
                        Queue.user_id,
                        Queue.request_time,
                        Queue.response,
                        User.lat,
                        User.lng,
                        User.vr_zone
                        FROM
                            users AS User
                        INNER JOIN queues AS Queue ON(
                            Queue.user_id = User.id                 
                        )
                        WHERE
                            (
                                (
                                    (
                                        
                                        User.running_distance IS NOT NULL
                                        AND(
                                            
                                                3963.0 * ACOS( SIN('$lat'  * PI() / 180)* SIN(User.lat * PI() / 180)+ COS('$lat' * PI() / 180)* COS(User.lat * PI() / 180)* COS(
                                                ('$lng' * PI()/ 180)-(User.lng * PI() / 180))) <= User.running_distance
                                        )
                                    )
                                )
                                OR(
                                    User.running_distance IS NULL
                                )
                            )
                        AND User.is_enabled = '1'
                        AND User.type = 'driver'
                        AND User.id <> '$requester'
                        AND User.vr_available = 'yes'
                        AND Queue.user_id NOT IN(SELECT user_id AS user_id FROM queues WHERE booking_id='$booking_id'
                                UNION ALL
                                    SELECT user_id AS user_id FROM queues WHERE response = 'accepted' OR (response IS NULL AND booking_id IS NOT NULL)
                                UNION ALL
                                    SELECT userId1 AS user_id FROM bars WHERE userId1 = Queue.user_id AND userId2 = '$requester'
                                UNION ALL
                                    SELECT userId2 AS user_id FROM bars WHERE userId1 = '$requester' AND userId2 = Queue.user_id) 
                        AND (SELECT SUM(amount) FROM transactions WHERE user_id = Queue.user_id 
                            AND service <> 'cash_in_hand' AND is_paid = 1) >= '$min_balance'
                        $car_passenger_cond_alt
                        GROUP BY Queue.user_id
                        LIMIT 1
                    )Driver 
                "
            );
        }
        if(!empty($driver)) {
            return $driver[0];          
        } else {
            return null;
        }

    }

 
      function _push($receiver_id, $message, $app, 
        $booking_id = null, 
        $phone = null, 
        $c = null, 
        $v = null, 
        $pick_up = null, 
        $passenger = null, 
        $position = null, 
        $zone = null, 
        $driver_pick = null, 
        $accept = null, 
        $emergency = null, 
        $driver_id = null, 
        $pn = null, 
        $gn = null,
        $g_position = null, 
        $g_zone = null,
        $call_position = null, 
        $call_zone = null){
        if($_SERVER['HTTP_HOST'] == 'localhost') return true;
        App::import('Vendor', 'UA', array('file' => 'UA' . DS . 'push.php'));
        $this->loadModel('DeviceToken');
        $devices = $this->DeviceToken->find('all', array(
                'recursive' => -1,
                'conditions' => array(
                    'DeviceToken.user_id' => $receiver_id
                )
            )
        );
        $sendPush = new SendPush();
        try {
            $sendPush->push($receiver_id, $message, $app, $devices, 
                $booking_id, $phone, $c, $v, $pick_up, $passenger, 
                $position, $zone, $driver_pick, $accept, $emergency, 
                $driver_id, $pn, $gn, $g_position, $g_zone, 
                $call_position, $call_zone);
        } catch(Exception $e){

        }
    }

    //sent verification code to user 
    function _sendSms($phoneNo, $text){
      //  require "Twilio.php";
        App::import('Vendor', 'Twilio', array('file' => 'sms/Twilio.php'));
        //require 
        // set your AccountSid and AuthToken from www.twilio.com/user/account
        $AccountSid = "ACd5880a6ed2193077c585763c514be2a2";
        $AuthToken = "c67580685ab040d98a11612441b87848";
         
        $client = new Services_Twilio($AccountSid, $AuthToken);
        
        try {
            $sms = $client->account->sms_messages->create(
                "+44 20 3095 7374", // From this number
                $phoneNo,// "+44 1576 820016", // To this number
                $text//"Test message!"
            );
        } catch(Exception $e){
            
        }
        
         //return $sms;
    }

    function _sendEmail($to, $subject, $content, $template = 'default', $viewVars = array(), $layout = 'default', $attachment = null){
            App::uses('CakeEmail', 'Network/Email');
            if(empty($viewVars)){
              $viewVars['title'] = $subject;                
            }
            $viewVars['body'] = $content;
            $email = new CakeEmail('smtp');
            $email->template($template, $layout);
            $email->viewVars($viewVars);
            $email->emailFormat('both');
            $email->subject($subject);
            $email->to($to);
            if(!empty($attachment)){
                $email->attachments($attachment);
            }
            try{
                $email->send();
            } catch(Exception $e){
                
            }
            
    }

    function _billing_twillio($id, $amount){
        $this->loadModel('Transaction');
        $data['Transaction']['user_id'] = $id;
        $data['Transaction']['amount'] = '-'.$amount;
        $data['Transaction']['short_description'] = 'billing_twillio';
        $data['Transaction']['service'] = 'billing_twillio';
        $now = date('Y-m-d');
        $data['Transaction']['timestamp'] = $now;
        $data['Transaction']['currency_code'] = CURRENCY;
        $this->Transaction->create();
        $this->Transaction->save($data, false);
    }

    function _position_driver($driver_zone = null, $omit= null){
        $this->loadModel('Booking');
        $this->loadModel('User');
        if(!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->Booking->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.vr_available = 'yes'
                        AND u.is_enabled = 1
                        AND u.vr_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET virtual_rank = '$position' WHERE id = '$driver_id'");
                if($driver_id != $omit && $this->User->getAffectedRows()){
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " .$position." in VR Zone.",'CabbieCall', null, null, null, null, null, null, $position, $zone_name);    
                }
                
            }
        }
    }

    function _position_driver_phone($driver_zone = null, $omit= null){
        $this->loadModel('PhoneQueue');
        $this->loadModel('User');
        if(!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->PhoneQueue->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            phone_queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.phonecall_available = 'yes'
                        AND u.is_enabled = '1'
                        AND u.phonecall_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET phone_rank = '$position' WHERE id = '$driver_id'");
                if($driver_id != $omit && $this->User->getAffectedRows()){
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " .$position." in Phone Call Zone.",'CabbieCall', 
                        null, null, null, null, null, null, null, null,
                        null, null, null, null, null, null, null, null, $position, $zone_name);   
                }
                
            }
        }
    }

    function _position_driver_gps($driver_zone = null, $omit= null){
        $this->loadModel('PhoneQueue');
        $this->loadModel('User');
        if(!empty($driver_zone)) {
            $position = 0;
            $drivers = $this->PhoneQueue->query("SELECT
                    t.*
                FROM
                    (
                        SELECT
                            q.*
                        FROM
                            phone_queues q
                        INNER JOIN users u ON(q.user_id = u.id)
                        WHERE
                            u.phonecall_gps_available = 'yes'
                        AND u.is_enabled = 1
                        AND u.phonecall_gps_zone = '$driver_zone'
                        ORDER BY
                            request_time DESC
                    )AS t
                GROUP BY
                    t.user_id
                ORDER BY
                    t.request_time");
            $this->loadModel('Zone');
            $zone = $this->Zone->findById($driver_zone, array('name'));
            $zone_name = $zone['Zone']['name'];
            foreach ($drivers as $dvr) {
                $driver_id = $dvr['t']['user_id'];
                $position = $position + 1;
                $this->User->query("UPDATE users SET gps_rank = '$position' WHERE id = '$driver_id'");
                if($driver_id != $omit && $this->User->getAffectedRows()){
                    // send notification to driver except for who got the job offer currently
                    $this->_push($driver_id, "Your current position is " .$position." in Phone GPS Zone.",'CabbieCall', 
                        null, null, null, null, null, null, null, 
                        null, null, null, null, null, null, null, $position, $zone_name);    
                }
                
            }
        }
    }
    
    public function rent_transaction($rent_item_id, $amount, $profit_percent){
        /*$this->autoRender = false;
        $this->loadModel('Payer');
        $this->loadModel('Payee');
        $this->loadModel('Transaction');
        $all_payers = $this->Payer->findAllByRentItemId($rent_item_id, 'Payer.user_id');
        $all_payees = $this->Payee->findAllByRentItemId($rent_item_id, 'Payee.user_id');
        $count_payers = $this->Payer->find('count', array(
            'conditions' => array('Payer.rent_item_id' => $rent_item_id)
        ));
        $count_payees = $this->Payee->find('count', array(
            'conditions' => array('Payee.rent_item_id' => $rent_item_id)
        ));
        $total_amount = $amount * $count_payers;
        $percent_amount = ($total_amount * $profit_percent)/ 100;
        $per_person_amount = $percent_amount / $count_payees;
        // print_r($per_person_amount);
        // print_r(' '.$count_payers);
        // print_r(' '.$count_payees); exit;
        foreach ($all_payers as $key => $payer) {
            $this->Transaction->create();
            $this->Transaction->save(array(
                'Transaction' => array(
                    'user_id' => $all_payers[$key]['Payer']['user_id'],
                    'rent_item_id' => $rent_item_id,
                    'short_description' => 'rent_fee',
                    'timestamp' => date('Y-m-d'),
                    'service' => 'rent_fee',
                    'amount' => '-'. $amount,
                    'currency_code' => CURRENCY,
                    'is_paid' => 0
                    )
                )
            );
        }
        foreach ($all_payees as $key => $payee) {
            $this->Transaction->create();
            $this->Transaction->save(array(
                'Transaction' => array(
                    'user_id' => $all_payees[$key]['Payee']['user_id'],
                    'rent_item_id' => $rent_item_id,
                    'short_description' => 'rent_profit',
                    'service' => 'rent_profit',
                    'timestamp' => date('Y-m-d'),
                    'amount' => $per_person_amount,
                    'currency_code' => CURRENCY,
                    'is_paid' => 0
                    )
                )
            );
        }*/
    }
}