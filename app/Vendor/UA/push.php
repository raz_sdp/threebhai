<?php
//require_once 'urbanairship.php';

class SendPush {

	//push message
	public function push($receiver_id, $msg='', $app = 'CabbieApp',  $devices,
	 $booking_id = null, 
	 $phone = null, 
	 $c = null, 
	 $v = null, 
	 $pick_up = null, 
	 $passenger = null, 
	 $position = null, 
	 $zone = null, 
	 $driver_pick = null, 
	 $accept = null, 
	 $emergency = null, 
	 $driver_id = null, 
	 $pn = null, 
	 $gn = null, 
	 $g_position = null, 
	 $g_zone = null, 
	 $call_position = null, 
	 $call_zone = null){
	 	
        // Put your private key's passphrase here:
        $passphrase = 'cabbieappuk';

        $push_url_development = 'ssl://gateway.sandbox.push.apple.com:2195';
        $push_url_production = 'ssl://gateway.push.apple.com:2195';
        $android_device_tokens = array();
        foreach ($devices as $device) {
        	$device_token = trim($device['DeviceToken']['device_token']);
        	if($device_token){
				if($app == 'CabbieApp') { // for CabbieApp [passengers] development mode
					// if(trim($device['DeviceToken']['stage']) == 'development'){
	    //                // $APP_MASTER_SECRET = 'ryXAM-HvQNi0QH0c9sfFLw';
	    //                // $APP_KEY = 'V8JZY835QIGwR_8di1-x-Q';
					// $certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'CabbieAppUKPDevCK.pem';
	    //             }else{// for CabbieApp [passengers] production mode
	    //                 // $APP_MASTER_SECRET = 'C2XMcbDOTu2BeQzuq0Ks1g';
	    //                 // $APP_KEY = 'RoJTW_xyRfi0ejcrDHqduw';
	    //                $certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'CabbieAppUKProCK.pem';
	    //             }
					$certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'CabbieAppUKProCK.pem';
	                $API_ACCESS_KEY = 'AIzaSyD4IctBYwPdeqQpVY5v7dbnbRALk7_Eu4E';
				} else { // for CabbieCall [drivers] development mode
					if(trim($device['DeviceToken']['stage']) == 'development'){
	                	// $APP_MASTER_SECRET = 'GhexrItAQVKZMsNCMGBHag';
	                	// $APP_KEY = 'prmkECkeR2yLuX1bFRZ_6w';
	                	$certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'CabbieAppUKDDevck.pem';
	                }else{// for CabbieCall [drivers] production mode
	                    // $APP_MASTER_SECRET = 'X5xXtRzURs26rCORpMW-cg';
	                    // $APP_KEY = 'LZE2aDQoTNWUuUZwp6HE7w';
	                   $certificate_path_production = WWW_ROOT . 'files' . DS . 'push_certificate' . DS . 'CabbieCallUKProCK.pem';
	                }
	                $API_ACCESS_KEY = 'AIzaSyDQ2ukpOhM7LGcBr15QaiJmKNJN7MQzcHs';
				}

				if($accept!== null) {
					$accept = var_export($accept, true);
				}
				if($c!== null) {
					$c = var_export($c, true);
				}
				if($v!== null) {
					$v = var_export($v, true);
				}
				if($pn!== null) {
					$pn = var_export($pn, true);
				}
				if($gn!== null) {
					$gn = var_export($gn, true);
				}
				if($driver_pick!== null){
					$driver_pick = var_export($driver_pick, true);	
				} 
				if($emergency!== null){
					$emergency = var_export($emergency, true);	
				}
				if($booking_id!== null){
					$booking_id = strval($booking_id);
				}
				if($passenger!== null){
					$passenger = strval($passenger);
				}
				if($position!== null){
					$position = strval($position);
				}
				if($call_position!== null){
					$call_position = strval($call_position);
				}
				if($g_position!== null){
					$g_position = strval($g_position);
				}
				if($driver_id!== null){
					$driver_id = strval($driver_id);
				}
				if($phone!== null){
					$phone = strval($phone);
				}

				if($app == 'CabbieApp'){
					if (!empty($driver_pick)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'DP' => $driver_pick
								)
							);
					} elseif (!empty($accept)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'a.wav',
									'badge' => '+1',
									'DA' => $accept
								)
							);
					} elseif (!empty($booking_id) && !empty($phone)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'B' => $booking_id,
									'P' => $phone,
								)
							);
					} else {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'default',
									'badge' => '+1',
								)
							);
					}
				} else { //driver app
					if(!empty($booking_id) && !empty($pick_up) && !empty($passenger)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'B' => $booking_id,
									'A' => $pick_up,
									'N' => $passenger
								)
							);
					} elseif (!empty($c) && !empty($booking_id)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'C' => $c,
									'B' => $booking_id
								)
							);
					} elseif (!empty($v)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'VN' => $v,
								)
							);
					}elseif (!empty($pn)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'PN' => $pn,
								)
							);
					}elseif (!empty($gn)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'badge' => '+1',
									'GN' => $gn,
								)
							);
					} elseif(!empty($position) && !empty($zone)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									// 'sound' => 'm.wav',
									'sound' => 'default',
									'badge' => '+1',
									'VRB' => $zone.' - P'.$position,
								)
							);
					} elseif(!empty($g_position) && !empty($g_zone)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									// 'sound' => 'm.wav',
									'sound' => 'default',
									'badge' => '+1',
									'GRB' => $g_zone.' - P'.$g_position,
								)
							);
					} elseif(!empty($call_position) && !empty($call_zone)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									// 'sound' => 'm.wav',
									'sound' => 'default',
									'badge' => '+1',
									'PRB' => $call_zone.' - P'.$call_position,
								)
							);
					} elseif (!empty($emergency) && !empty($driver_id)) {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'e.wav',
									'badge' => '+1',
									'E' => $emergency,
									'D' => $driver_id
								)
							);
					} else {
						$message = array(
								'aps'=>array(
									'alert'=> $msg,
									'sound' => 'w.wav',
									'default1' => true,
									'badge' => '+1'
								)
							);
					}
				}
				
				//$airship = new Airship($APP_KEY, $APP_MASTER_SECRET);
				if(trim($device['DeviceToken']['device_type']) == 'ios'){
					//$res = $airship->push($message, $device_token);	
					//if($device['DeviceToken']['stage'] != 'production'){
                    //    $this->_apns($certificate_path_development, $passphrase, $push_url_development, $device_token, $message);
                    //} elseif ($device['DeviceToken']['stage'] == 'production') {
                       $this->_apns($certificate_path_production, $passphrase, $push_url_production, $device_token, $message);
                    //}
				} elseif(trim($device['DeviceToken']['device_type']) == 'android'){
					//print_r($message);
					//$res = $airship->push($message);
					array_push($android_device_tokens, $device_token);
				}
				
	        }
		}
		if(count($android_device_tokens) > 0) {
          $this->_agcm($android_device_tokens, $API_ACCESS_KEY, $message);
          //$this->_agcm($android_device_tokens, 'AIzaSyD4IctBYwPdeqQpVY5v7dbnbRALk7_Eu4E', $message);
        }
	}



private function _apns($certificate_path, $passphrase, $push_url, $deviceToken, $body){
	// echo "path: " . $certificate_path . " pass: " . $passphrase;
      $ctx = stream_context_create();
      stream_context_set_option($ctx, 'ssl', 'local_cert', $certificate_path);
      stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

      // Open a connection to the APNS server

      $fp = stream_socket_client($push_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
      if (!$fp) exit(0); 
      // exit("Failed to connect: $err $errstr" . PHP_EOL);
      // echo 'Connected to APNS' . PHP_EOL;

      // Encode the payload as JSON

      $payload = json_encode($body);

      // Build the binary notification

      $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

      // Send it to the server

      $result = fwrite($fp, $msg, strlen($msg));
       if (!$result) echo 'Message not delivered' . PHP_EOL;
       else echo 'Message successfully delivered' . PHP_EOL;
      //print_r($result);
      // Close the connection to the server
      fclose($fp);
    }

    private function _agcm($registrationIds, $API_ACCESS_KEY, $getMessage){
      //define( 'API_ACCESS_KEY', 'AIzaSyBIB74xCLAT1veUkLkge9fd6c4J40QZ48o' );
 
      //$registrationIds = array( $_GET['id'] );
       
      // prep the bundle
      $msg = array
      (
        'message'     => $getMessage,
        'sound'   => 1
      );
       
      $fields = array
      (
        'registration_ids'  => $registrationIds,
        'data'        => $msg
      );
       
      $headers = array
      (
        'Authorization: key=' . $API_ACCESS_KEY,
        'Content-Type: application/json'
      );
       
      $options = array(
        'http'=>array(
          'method' => 'POST',
          'header' =>
            'Authorization: key=' . $API_ACCESS_KEY . "\r\n".
          'Content-Type: application/json',
          'content' => json_encode( $fields )
      ));

      $context = stream_context_create($options);

      $result = file_get_contents('https://android.googleapis.com/gcm/send', false, $context);

      //echo $result;
       
    }
}