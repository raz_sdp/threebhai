<?php
App::uses('AppModel', 'Model');
/**
 * Queue Model
 *
 * @property Zone $Zone
 * @property User $User
 */
class Queue extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Zone' => array(
			'className' => 'Zone',
			'foreignKey' => 'zone_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		,
		'Booking' => array(
			'className' => 'Booking',
			'foreignKey' => 'booking_id',
			'conditions' => '',//array('Queue.booking_id' => 'Booking.id'),
			'fields' => '',
			'order' => ''
		),
		'DriverDeclineReason' => array(
            'className' => 'DriverDeclineReason',
            'foreignKey' => 'driver_decline_reason_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);



    public function get_timeout_queues(){
    	$this->recursive = -1;
        $timeout_list = $this->find('all', array(
            'conditions' => array(
                'response' => 'timeout'
            )
        ));
        return $timeout_list;
    }

/*
 *	Edit queue entry when driver cancel a booking
 */	
	public function make_cancelled($driver_id, $booking_id) {
		if(empty($driver_id) || empty($booking_id)) return false;
		$queue_id = $this->field('id', array(
			'Queue.user_id' => $driver_id, 'Queue.booking_id' => $booking_id
		));
		$this->id = $queue_id;
		$this->saveField('response', 'cancelled');
		$this->query("
					UPDATE bookings
						SET status = 'cancelled'
						WHERE
						id = '$booking_id'
					");	
		return true;
	}

  


} 
