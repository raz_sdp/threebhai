<?php
App::uses('AppModel', 'Model');
/**
 * DriverDeclineReason Model
 *
 */
class DriverDeclineReason extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'reason';
	
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'reason' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	
	public function get_reasons() {
		$decline_reasons = $this->find('list', array(
				'conditions' => array('is_active' => '1')
			));
		return $decline_reasons;
	}

}
