<?php
App::uses('AppModel', 'Model');
/**
 * ArchivedBooking Model
 *
 * @property Zone $Zone
 */
class ArchivedBooking extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'pick_up';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Zone' => array(
			'className' => 'Zone',
			'foreignKey' => 'zone_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Requester' => array(
			'className' => 'User',
			'foreignKey' => 'requester',
			'conditions' => '',
			'fields' => array('id', 'name', 'email', 'mobile', 'lat', 'lng','type'),
			'order' => ''
		),
		'Acceptor' => array(
			'className' => 'User',
			'foreignKey' => 'acceptor',
			'conditions' => '',
			'fields' => array('id', 'name', 'email', 'mobile', 'lat', 'lng', 'type','badge_no'),
			'order' => ''
		),
	);

}
