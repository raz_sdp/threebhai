<?php
App::uses('AppModel', 'Model');
/**
 * Transaction Model
 *
 * @property User $User
 * @property ReferredUser $ReferredUser
 * @property App $App
 * @property Booking $Booking
 * @property RentItem $RentItem
 */
class Transaction extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		// 'ReferredUser' => array(
		// 	'className' => 'ReferredUser',
		// 	'foreignKey' => 'referred_user_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// ),
		// 'App' => array(
		// 	'className' => 'App',
		// 	'foreignKey' => 'app_id',
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => ''
		// ),
		'Booking' => array(
			'className' => 'Booking',
			'foreignKey' => 'booking_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RentItem' => array(
			'className' => 'RentItem',
			'foreignKey' => 'rent_item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function getDriverBalance($user_id) {
		$sum_amount = $this->query(" SELECT
			SUM(amount)AS total,
			'total' AS type
		FROM
			transactions
		WHERE
			user_id = '$user_id'
		AND short_description IN 
			('taxi_fare', 
			'top_up', 
			'promoter_profit',
			'rent_profit',
			'transfer_paid') 
		AND service <> 'cash_in_hand'
		AND is_paid = 1
		UNION ALL
			SELECT
				SUM(amount)AS total,
				'free_credit' AS type
			FROM
				transactions
			WHERE
				user_id = '$user_id'
			AND short_description = 'free_credit'
			AND is_paid = 1
			UNION ALL
				SELECT
					SUM(amount)AS total,
					'total_fee' AS type
				FROM
					transactions
				WHERE
					user_id = '$user_id'
				AND short_description IN ('admin_fee','promoter_fee','billing_twillio', 'rent_fee')
				AND is_paid = 1
				UNION ALL
					SELECT
						SUM(amount)AS total,
						'actual balance' AS type
					FROM
						transactions
					WHERE
						user_id = '$user_id'
					AND service <> 'cash_in_hand'
					AND short_description <> 'free_credit'
					AND is_paid = 1
					UNION ALL
						SELECT
							SUM(amount)AS total,
							'taxi_fare & top_up' AS type
						FROM
							transactions
						WHERE
							user_id = '$user_id' 
							AND short_description IN 
								('taxi_fare', 
								'top_up', 'promoter_profit', 
								'rent_profit',
								'transfer_pending', 
								'transfer_paid') 
							AND service <> 'cash_in_hand'
							AND is_paid = 1
			");
			return $sum_amount;
	}

	public function getPassengerBalance($user_id) {
		$sum_amount = $this->query("SELECT SUM(amount) AS total FROM transactions WHERE user_id = '$user_id' GROUP BY user_id ");
		return $sum_amount;

	}

	public function getAdminEarning($admin_percentage = null, $taxi_fare = null) {
		$admin_amount = ($taxi_fare * $admin_percentage) /100;
		return $admin_amount;
	}

	public function calFreeCredit($free_credit = null, $total_fee = null, $total = null){
		$remaining_free_credit = 0;
		if($free_credit <= $total_fee) {
			//$left_fc = 0;
			$remaining_free_credit = 0;
			$fee_paid_from_earning = $total_fee - $free_credit;
			$total = $total - $fee_paid_from_earning;
		} else {
			$remaining_free_credit = $free_credit - $total_fee;
		}
	
		return array('remaining_free_credit' => $remaining_free_credit , 'total' => $total);
	}
	
}
