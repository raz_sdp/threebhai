<?php
App::uses('AppModel', 'Model');
/**
 * Zone Model
 *
 * @property Queue $Queue
 */
class Zone extends AppModel {

	public $displayField = 'name';
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'polygon_points' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				'allowEmpty' => true,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	//	'voip_phone_no' => array(
	//		'notempty' => array(
	//			'rule' => array('/^(?(?:(?:0(?:0|11))?[\s-]?(?+?|+)?(44))?[\s-]?(?(?:0)?[\s-]?(?)?|0)?[\s-]?(?)?((?:1[1-9]|2[03489]|3[0347]|5[056]|7[04-9]|8[047]|9[018])(?:)?[\s-]?\d){4}(?:[\s-]?\d){3,4})$/'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
	//		),
	//	),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Queue' => array(
			'className' => 'Queue',
			'foreignKey' => 'zone_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'VrSetting' => array(
			'className' => 'VrSetting',
			'foreignKey' => 'zone_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PhoneQueue' => array(
			'className' => 'PhoneQueue',
			'foreignKey' => 'zone_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
	);

// hasAndBelongsToMany associations

	public $hasAndBelongsToMany = array(
		'User' => array(
			'className' => 'User',
			'joinTable' => 'users_zones',
			'foreignKey' => 'zone_id',
			'associationForeignKey' => 'user_id',
			'unique' => 'keepExisting',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
		)
	);
	
    public function getZone($lat, $lng){
        $zone = $this->find('first',array(
                'recursive' => -1,
                'conditions' => array(
                    "CONTAINS (GEOMFROMTEXT(Zone.polygon), GEOMFROMTEXT('POINT($lat $lng)'))"
                ),
                'fields' => array(
                    'Zone.id', 'Zone.name'
                )
            )
        );
        return $zone;
    }
	
}
