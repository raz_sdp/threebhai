<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 12/28/16
 * Time: 4:18 PM
 */
App::uses('AppModel', 'Model');
/**
 * DeviceToken Model
 *
 * @property User $User
 */
class HomePage extends AppModel {
    public $useTable = 'home_page';
}