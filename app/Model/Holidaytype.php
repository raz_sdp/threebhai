<?php
App::uses('AppModel', 'Model');
/**
 * Holidaytype Model
 *
 * @property Holiday $Holiday
 */
class Holidaytype extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'holiday_type';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Holiday' => array(
			'className' => 'Holiday',
			'foreignKey' => 'holidaytype_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('Holiday.holiday' => 'ASC'),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
