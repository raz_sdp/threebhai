<?php
App::uses('AppModel', 'Model');
/**
 * DeviceToken Model
 *
 * @property User $User
 */
class CompanyInformation extends AppModel {
    public $useTable = 'company_information';
}