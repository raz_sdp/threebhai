
<?php
App::uses('AppModel', 'Model');
/**
 * VrSetting Model
 *
 * @property Zones $Zones
 */
class VrSetting extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'is_advance_booking_on' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), 
        'start_time' => array(
            'time' => array(
                'rule' => array('time'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'end_time' => array(
            'time' => array(
                'rule' => array('time'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
		/*'zone_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),*/
	/*	'is_over_job_notification' => array(
			'boolean' => array(
				'rule' => array('boolean'),
				//'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		), */
       /* 'min_advance_booking_time' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),*/
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Zone' => array(
			'className' => 'Zone',
			'foreignKey' => 'zone_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
        'Holidaytype' => array(
            'className' => 'Holidaytype',
            'foreignKey' => 'holidaytype_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);

/** 
* Get the nearest driver's driving time
*/
    public function get_driving_time($s_lat, $s_lng, $d_lat, $d_lng){
        App::uses('HttpSocket', 'Network/Http');

        $HttpSocket = new HttpSocket();

        // array query
        $results = $HttpSocket->get('http://maps.googleapis.com/maps/api/directions/json', array(
            'origin' => $s_lat .','.  $s_lng, 
            'destination' => $d_lat .','.  $d_lng,
            'sensor' => 'false',
            'mode' => 'driving'

        ));

         
         $json = json_decode($results->body, true);
         return $json['routes'][0]['legs'][0]['duration']['value'];

         //return $results;
    }
}
